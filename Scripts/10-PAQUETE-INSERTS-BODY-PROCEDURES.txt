create or replace package body pck_insert as

procedure p_insert_canton (p_canton_name in varchar2, p_province_id in number) 
as begin
insert into tbl_canton(canton_id, name, province_id)
values (s_canton.nextval, p_canton_name, p_province_id);
commit;
end p_insert_canton;



procedure p_insert_category (p_category_name in varchar2) 
as begin
insert into tbl_category(category_id, category_name)
values (s_category.nextval, p_category_name);
commit;
end p_insert_category;



procedure p_insert_country (p_country_name in varchar2) 
as begin
insert into tbl_country(country_id, name)
values (s_country.nextval, p_country_name);
commit;
end p_insert_country;



procedure p_insert_district (p_district_name in varchar2, p_canton_id in number) 
as begin
insert into tbl_district(district_id, name, canton_id)
values (s_district.nextval, p_district_name, p_canton_id);
commit;
end p_insert_district;



procedure p_insert_email (p_email in varchar2, p_person_id in number) 
as begin
insert into tbl_email(email_id, email, person_id)
values (s_email.nextval, p_email, p_person_id);
commit;
end p_insert_email;



procedure p_insert_favourite (p_name in varchar2, p_category_id in number) 
as begin
insert into tbl_favourite(favourite_id, favourite_name, category_id)
values (s_favourite.nextval, p_name, p_category_id);
commit;
end p_insert_favourite;



procedure p_insert_forum (p_name in varchar2, p_canton_id in number) 
as begin
insert into tbl_forum(forum_id, forum_name, canton_id)
values (s_forum.nextval, p_name, p_canton_id);
commit;
end p_insert_forum;



procedure p_insert_message (p_text in varchar2, p_publishdate in varchar2, p_username in varchar2, p_project_id in number) 
as begin
insert into tbl_message(message_id, text, publish_date, username, project_id)
values (s_message.nextval, p_text, to_date(p_publishdate, 'yyyy/mm/dd') , p_username, p_project_id);
commit;
end p_insert_message;



procedure p_insert_nationality (p_name in varchar2) 
as begin
insert into tbl_nationality(nationality_id, nationality_name)
values (s_nationality.nextval, p_name);
commit;
end p_insert_nationality;


procedure p_insert_parameter (p_value in varchar2) 
as begin
insert into tbl_parameter(parameter_id, parameter_value)
values (s_parameter.nextval, p_value);
commit;
end p_insert_parameter;



procedure p_insert_person (p_identification in varchar2, p_name in varchar2, p_firstlastname in varchar2, 
p_secondlastname in varchar2, p_birthdate in varchar2, p_picture in varchar2, p_username varchar2, p_district_id in number, 
p_nationality_id in number, p_email in varchar2, p_phonenumber in number) 
as begin
insert into tbl_person(person_id, identification, name, first_lastname, second_lastname, birthdate, picture, username, district_id)
values (s_person.nextval, p_identification, p_name, p_firstlastname, p_secondlastname, to_date(p_birthdate, 'yyyy/mm/dd'), p_picture, p_username, p_district_id);

insert into tbl_personxnationality(person_id, nationality_id)
values (s_person.currval, p_nationality_id);

insert into tbl_email(email_id, email, person_id)
values (s_email.nextval, p_email, s_person.currval);

insert into tbl_phonenumber(phonenumber_id, phonenumber, person_id)
values (s_phonenumber.nextval, p_phonenumber, s_person.currval);

commit;
end p_insert_person;



procedure p_insert_phonenumber (p_phonenumber in number, p_person_id in number) 
as begin
insert into tbl_phonenumber(phonenumber_id, phonenumber, person_id)
values (s_phonenumber.nextval, p_phonenumber, p_person_id);
commit;
end p_insert_phonenumber;




procedure p_insert_project (p_projectname in varchar2, p_summary in varchar2, p_budget in number, 
p_votes in number, p_username in varchar2, p_category_id in number, p_forum_id number, p_projectdate in varchar2) 
as begin
insert into tbl_project(project_id, project_name, summary, budget, votes, username, category_id, forum_id, project_date)
values (s_project.nextval, p_projectname, p_summary, p_budget, p_votes, p_username, p_category_id, p_forum_id, to_date(p_projectdate, 'yyyy/mm/dd'));
commit;
end p_insert_project;


procedure p_insert_province (p_province_name in varchar2, p_country_id in number) 
as begin
insert into tbl_province(province_id, name, country_id)
values (s_province.nextval, p_province_name, p_country_id);
commit;
end p_insert_province;



procedure p_insert_user (p_username in varchar2, p_password in varchar2, p_type_id in number) 
as begin
insert into tbl_user(username, password, type_id)
values (p_username, p_password, p_type_id);
commit;
end p_insert_user;


procedure p_insert_usertype (p_usertype_name in varchar2) 
as begin
insert into tbl_usertype(usertype_id, usertype_name)
values (s_usertype.nextval, p_usertype_name);
commit;
end p_insert_usertype;

end;