﻿using Citizen.Data_Access;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class ProvinceBUS {
        private ProvinceDAO provinceDAO;

        public ProvinceBUS() {
            provinceDAO = new ProvinceDAO();
        }

        //public Province GetProvinceByCountryID(int id) {
        //    Province province = new Province();
        //    DataTable table = provinceDAO.SearchProvinceByCountryID(id);
        //    foreach (DataRow dr in table.Rows) {
        //        province.Province_id = int.Parse(dr["province_id"].ToString());
        //        province.Province_name = dr["name"].ToString();
        //        province.Country_id = int.Parse(dr["country_id"].ToString());
        //        province.Creation_date = dr["creation_date"].ToString();
        //        province.Creation_user = dr["creation_user"].ToString();
        //        province.Last_modified = dr["last_modified"].ToString();
        //        province.Last_modified_user = dr["last_modified_user"].ToString();
        //    }
        //    return province;
        //}

        public ArrayList GetProvincesByCountryID(int id) {
            ArrayList prv = new ArrayList();
            
            DataTable table = provinceDAO.SearchProvinceByCountryID(id);
            foreach (DataRow dr in table.Rows) {
                Province province = new Province {
                    Id = int.Parse(dr["province_id"].ToString()),
                    Name = dr["name"].ToString(),
                    Country_id = int.Parse(dr["country_id"].ToString()),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString()
                };
                prv.Add(province);
            }

            return prv;
        }

        internal IEnumerable<Province> GetProvinces() {
            List<Province> cts = new List<Province>();
            DataTable table = provinceDAO.SearchAll();
            foreach (DataRow dr in table.Rows) {
                Province province = new Province {
                    Id = int.Parse(dr["province_id"].ToString()),
                    Name = dr["name"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString()
                };
                cts.Add(province);
            }

            return cts;
        }

        internal void DeleteProvince(int v) {
            provinceDAO.Delete(v);
        }
    }
}
