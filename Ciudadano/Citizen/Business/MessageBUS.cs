﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class MessageBUS {
        private MessageDAO messageDAO;

        public MessageBUS() {
            messageDAO = new MessageDAO();
        }

        public List<Message> GetMessagesByProjectID(int id) {
            List<Message> msg = new List<Message>();
            DataTable table = messageDAO.Search("project_id", id);
            foreach (DataRow dr in table.Rows) {
                msg.Add(new Message() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Message_id = int.Parse(dr["Message_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Publish_date = dr["Publish_date"].ToString(),
                    Text = dr["Text"].ToString(),
                    Username = dr["Username"].ToString()
                });
            }
            return msg;
        }

        internal void InsertMessage(Message msg) {
            messageDAO.Insert(msg);
        }
    }
}
