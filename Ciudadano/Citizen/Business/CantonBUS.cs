﻿using Citizen.Data_Access;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class CantonBUS {
        private CantonDAO cantonDAO;

        public CantonBUS() {
            cantonDAO = new CantonDAO();
        }

        public Canton GetCantonByDistrictID(int id) {
            Canton canton = new Canton();
            DataTable dt = cantonDAO.SearchCantonByDistrictID(id);
            foreach (DataRow dr in dt.Rows) {
                canton.Province_id = int.Parse(dr["province_id"].ToString());
                canton.Canton_name = dr["name"].ToString();
                canton.Canton_id = int.Parse(dr["canton_id"].ToString());
                canton.Creation_date = dr["creation_date"].ToString();
                canton.Creation_user = dr["creation_user"].ToString();
                canton.Last_modified_user = dr["last_modified_user"].ToString();
            }
            return canton;
        }

        public ArrayList GetCantonByProvinceID(int id) {
            ArrayList cnt = new ArrayList();

            DataTable table = cantonDAO.SearchCantonByProvinceID(id);
            foreach (DataRow dr in table.Rows) {
                Canton canton = new Canton {
                    Province_id = int.Parse(dr["province_id"].ToString()),
                    Canton_name = dr["name"].ToString(),
                    //Console.WriteLine(province.Province_name);
                    Canton_id = int.Parse(dr["canton_id"].ToString()),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString()
                };
                cnt.Add(canton);
            }

            return cnt;
        }


        public List<Canton> GetCantons() {
            List<Canton> cts = new List<Canton>();
            DataTable table = cantonDAO.SearchAll();
            foreach (DataRow dr in table.Rows) {
                Canton canton = new Canton {
                    Id = int.Parse(dr["canton_id"].ToString()),
                    Name = dr["name"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString()
                };
                cts.Add(canton);
            }

            return cts;
        }

        internal void DeleteCanton(int v) {
            cantonDAO.Delete(v);
        }
    }
}
