﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class DistrictBUS {
        private DistrictDAO districtDAO;

        public DistrictBUS() {
            districtDAO = new DistrictDAO();
        }

        internal IEnumerable<District> GetDistricts() {
            List<District> cts = new List<District>();
            DataTable table = districtDAO.SearchAllDistricts();
            foreach (DataRow dr in table.Rows) {
                District district = new District {
                    Id = int.Parse(dr["district_id"].ToString()),
                    Name = dr["name"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString()
                };
                cts.Add(district);
            }

            return cts;
        }

        internal void DeleteDistrict(int v) {
            districtDAO.Delete(v);
        }

        //public ArrayList GetProvincesByCountryID(int id) {
        //    ArrayList prv = new ArrayList();

        //    DataTable table = provinceDAO.SearchProvinceByCountryID(id);
        //    foreach (DataRow dr in table.Rows) {
        //        Province province = new Province();
        //        province.Province_id = int.Parse(dr["province_id"].ToString());
        //        province.Province_name = dr["name"].ToString();
        //        Console.WriteLine(province.Province_name);
        //        province.Country_id = int.Parse(dr["country_id"].ToString());
        //        province.Creation_date = dr["creation_date"].ToString();
        //        province.Creation_user = dr["creation_user"].ToString();
        //        province.Last_modified = dr["last_modified"].ToString();
        //        province.Last_modified_user = dr["last_modified_user"].ToString();
        //        prv.Add(province);
        //    }

        //    return prv;
        //}


    }
}
