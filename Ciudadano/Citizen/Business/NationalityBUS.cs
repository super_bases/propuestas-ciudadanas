﻿using Citizen.Data_Access;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class NationalityBUS {

        private NationalityDAO nationalityDAO;

        public NationalityBUS() {
            nationalityDAO = new NationalityDAO();
        }

        public ArrayList GetNationalities() {
            ArrayList nat = new ArrayList();

            DataTable table = nationalityDAO.SearchNationalities();
            foreach (DataRow dr in table.Rows) {
                Nationality nationality = new Nationality {
                    Nationality_id = int.Parse(dr["nationality_id"].ToString()),
                    Nationality_name = dr["nationality_name"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Status = int.Parse(dr["status"].ToString())
                };
                nat.Add(nationality);
            }

            return nat;
        }

        internal void DeleteNationality(int v) {
            nationalityDAO.Delete(v);
        }

        internal void InsertNationality(string text) {
            nationalityDAO.Insert(text);
        }

        internal void EditNationality(int v, string text, int status) {
            nationalityDAO.Edit(v, text, status);
        }

        internal Nationality GetNationalityByID(int id) {
            Nationality nationality = new Nationality();
            DataTable table = nationalityDAO.SearchNationalityByID(id);
            foreach (DataRow dr in table.Rows) {
                nationality.Nationality_id = int.Parse(dr["nationality_id"].ToString());
                nationality.Nationality_name = dr["nationality_name"].ToString();
                nationality.Creation_date = dr["creation_date"].ToString();
                nationality.Creation_user = dr["creation_user"].ToString();
                nationality.Last_modified = dr["last_modified"].ToString();
                nationality.Last_modified_user = dr["last_modified_user"].ToString();
                nationality.Status = int.Parse(dr["status"].ToString());
            }
            return nationality;
        }
    }
}
