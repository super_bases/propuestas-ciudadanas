﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class ProjectBUS {
        private ProjectDAO projectDAO;

        public ProjectBUS() {
            projectDAO = new ProjectDAO();
        }

        public Project GetProjectByID(int id) {
            Project project = new Project();
            DataTable table = projectDAO.Search("project_id", id.ToString());
            foreach (DataRow dr in table.Rows) {
                project.Last_modified = dr["last_modified"].ToString();
                project.Last_modified_user = dr["last_modified_user"].ToString();
                project.Creation_date = dr["creation_date"].ToString();
                project.Creation_user = dr["creation_user"].ToString();
                project.Budget = float.Parse(dr["budget"].ToString());
                project.Category_id = int.Parse(dr["Category_id"].ToString());
                project.Forum_id = int.Parse(dr["Forum_id"].ToString());
                project.Project_id = int.Parse(dr["Project_id"].ToString());
                project.Project_name = dr["Project_name"].ToString();
                project.Summary = dr["Summary"].ToString();
                project.Username = dr["Username"].ToString();
                project.Votes = int.Parse(dr["Votes"].ToString());
            }
            return project;
        }

        public List<Project> GetProjectsByForumID(int id) {
            List<Project> projects = new List<Project>();
            DataTable table = projectDAO.Search("forum_id", id.ToString());
            foreach (DataRow dr in table.Rows) {
                projects.Add(new Project() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Budget = float.Parse(dr["budget"].ToString()),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Project_name = dr["Project_name"].ToString(),
                    Summary = dr["Summary"].ToString(),
                    Username = dr["Username"].ToString(),
                    Votes = int.Parse(dr["Votes"].ToString())
                });
            }
            return projects;
        }

        internal List<Project> GetProjectsByCategoryName(string selectedText) {
            List<Project> projects = new List<Project>();
            DataTable table = projectDAO.SearchByCategoryName(selectedText);
            foreach (DataRow dr in table.Rows) {
                projects.Add(new Project() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Budget = float.Parse(dr["budget"].ToString()),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Project_name = dr["Project_name"].ToString(),
                    Summary = dr["Summary"].ToString(),
                    Username = dr["Username"].ToString(),
                    Votes = int.Parse(dr["Votes"].ToString())
                });
            }
            return projects;
        }

        internal List<Project> GetProjectsInDateRange(string v1, string v2) {
            List<Project> projects = new List<Project>();
            DataTable table = projectDAO.DateRange(v1, v2);
            foreach (DataRow dr in table.Rows) {
                projects.Add(new Project() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Budget = float.Parse(dr["budget"].ToString()),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Project_name = dr["Project_name"].ToString(),
                    Summary = dr["Summary"].ToString(),
                    Username = dr["Username"].ToString(),
                    Votes = int.Parse(dr["Votes"].ToString())
                });
            }

            foreach (Project pr in projects)
                Debug.WriteLine(string.Format("{0} {1}", pr.Project_name, pr.Creation_date));
            return projects;
        }

        internal void InsertVote(Project pr) {
            pr.Votes++;
            projectDAO.InsertVote(pr);
        }

        internal void InsertProject(Project project) {
            projectDAO.Insert(project);
        }

        internal List<Project> GetProjectsByCategory(int cat) {
            List<Project> projects = new List<Project>();
            DataTable table = projectDAO.SearchByCategory(cat);
            foreach (DataRow dr in table.Rows) {
                projects.Add(new Project() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Budget = float.Parse(dr["budget"].ToString()),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Project_name = dr["Project_name"].ToString(),
                    Summary = dr["Summary"].ToString(),
                    Username = dr["Username"].ToString(),
                    Votes = int.Parse(dr["Votes"].ToString())
                });
            }
            return projects;
        }

        internal List<Project> GetProjectsByUsername(string username) {
            List<Project> projects = new List<Project>();
            DataTable table = projectDAO.SearchMyProjects(username);
            foreach (DataRow dr in table.Rows) {
                projects.Add(new Project() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Budget = float.Parse(dr["budget"].ToString()),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Project_id = int.Parse(dr["Project_id"].ToString()),
                    Project_name = dr["Project_name"].ToString(),
                    Summary = dr["Summary"].ToString(),
                    Username = dr["Username"].ToString(),
                    Votes = int.Parse(dr["Votes"].ToString())
                });
            }
            return projects;
        }
    }
}
