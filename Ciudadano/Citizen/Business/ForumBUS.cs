﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class ForumBUS {
        private ForumDAO forumDAO;

        public ForumBUS() {
            forumDAO = new ForumDAO();
        }

        public Forum GetForumByCantonID(int id) {
            Forum forum = new Forum();
            DataTable table = forumDAO.SearchByCantonID(id);
            foreach (DataRow dr in table.Rows) {
                forum.Last_modified = dr["last_modified"].ToString();
                forum.Last_modified_user = dr["last_modified_user"].ToString();
                forum.Creation_date = dr["creation_date"].ToString();
                forum.Creation_user = dr["creation_user"].ToString();
                forum.Forum_id = int.Parse(dr["Forum_id"].ToString());
                forum.Forum_name = dr["Forum_name"].ToString();
                forum.Canton_id = int.Parse(dr["canton_id"].ToString());
            }
            return forum;
        }

        internal IEnumerable<Forum> GetAll() {
            List<Forum> forum = new List<Forum>();
            DataTable table = forumDAO.GetAll();
            foreach (DataRow dr in table.Rows) {
                forum.Add(new Forum() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Forum_id = int.Parse(dr["Forum_id"].ToString()),
                    Forum_name = dr["Forum_name"].ToString(),
                    Canton_id = int.Parse(dr["canton_id"].ToString()),
                });
            }
            return forum;
        }
    }
}
