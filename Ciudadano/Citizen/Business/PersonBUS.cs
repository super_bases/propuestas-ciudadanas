﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class PersonBUS {

        private PersonDAO personDAO;

        public PersonBUS() {
            personDAO = new PersonDAO();
        }

        public void InsertPerson(string identification, string name,
                             string flastname, string slastname, string birthdate, string picture,
                             string username, int district_id, int nationality_id, string email, int phonenumber) {
            personDAO.InsertPerson(identification, name, flastname, slastname, birthdate, picture, username, district_id, nationality_id, email, phonenumber);
        }

        public void UpdatePerson(int id, string identification, string name, string flastname, string slastname,
                                 string birthdate, string picture, int district_id, int nationality_id,
                                 int email_id, string email, int phone_id, int phonenumber) {
            personDAO.UpdatePerson(id, identification, name, flastname, slastname, birthdate, picture, district_id, nationality_id, email_id, email, phone_id, phonenumber);
        }

        public void UpdatePassWord(string username, string newPass) {
            personDAO.UpdatePassWord(username, newPass);
        }

        /*
         p_person_id in number, p_identification in varchar2, p_name in varchar2, p_firstlastname in varchar2, 
p_secondlastname in varchar2, p_birthdate in varchar2, p_picture in varchar2, p_district_id in number, 
p_nationality_id in number, p_email_id in number, p_email in varchar2, p_phonenumber_id in number, p_phonenumber in number)
             */

        public void InsertUser(string username, string pass, int type) {
            personDAO.InsertUser(username, pass, type);
        }

        public Person GetPersonByUsername(string username) {
            Person person = new Person();
            DataTable table = personDAO.SearchByUsername(username);
            foreach (DataRow dr in table.Rows) {
                person.Person_id = int.Parse(dr["person_id"].ToString());
                person.Identification = dr["identification"].ToString();
                person.Name = dr["name"].ToString();
                person.First_lastname = dr["first_lastname"].ToString();
                person.Second_lastname = dr["second_lastname"].ToString();
                person.Date = dr["birthdate"].ToString();
                person.Picture = dr["picture"].ToString();
                person.Username = dr["username"].ToString();
                person.District_id = int.Parse(dr["district_id"].ToString());
                person.Last_modified = dr["last_modified"].ToString();
                person.Last_modified_user = dr["last_modified_user"].ToString();
                person.Creation_date = dr["creation_date"].ToString();
                person.Creation_user = dr["creation_user"].ToString();
            }
            return person;
        }


        internal List<Person> GetAll() {
            List<Person> person = new List<Person>();
            DataTable table = personDAO.SearchAllPersons();
            foreach (DataRow dr in table.Rows) {
                person.Add(new Person() {
                    Person_id = int.Parse(dr["person_id"].ToString()),
                    Identification = dr["identification"].ToString(),
                    Name = dr["name"].ToString(),
                    First_lastname = dr["first_lastname"].ToString(),
                    Second_lastname = dr["second_lastname"].ToString(),
                    Date = dr["birthdate"].ToString(),
                    Picture = dr["picture"].ToString(),
                    Username = dr["username"].ToString(),
                    District_id = int.Parse(dr["district_id"].ToString()),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString()
                });

            }
            return person;
        }

        internal int CountAgeRange(int start, int end) {
            int count = 0;
            DataTable table = personDAO.AgeRange(start, end);
            foreach (DataRow dr in table.Rows)
                count = int.Parse(dr["count(person_id)"].ToString());
            return count;
        }

    }
}
