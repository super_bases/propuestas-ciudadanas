﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class ParameterBUS {
        private ParameterDAO parameterDAO;

        public ParameterBUS() {
            parameterDAO = new ParameterDAO();
        }
        public Parameter GetParameter() {
            Parameter parameter = new Parameter();
            DataTable table = parameterDAO.Search();
            foreach (DataRow dr in table.Rows) {
                parameter.Last_modified = dr["last_modified"].ToString();
                parameter.Last_modified_user = dr["last_modified_user"].ToString();
                parameter.Creation_date = dr["creation_date"].ToString();
                parameter.Creation_user = dr["creation_user"].ToString();
                parameter.Parameter_id = int.Parse(dr["Parameter_id"].ToString());
                parameter.Parameter_value = dr["Parameter_value"].ToString();
            }
            return parameter;
        }
    }
}
