﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class CategoryBUS {
        private CategoryDAO categoryDAO;

        public CategoryBUS() {
            categoryDAO = new CategoryDAO();
        }

        public Category GetCategoryByName(string name) {
            Category category = new Category();
            DataTable table = categoryDAO.GetCategoryByName(name);
            foreach (DataRow dr in table.Rows) {
                category.Last_modified = dr["last_modified"].ToString();
                category.Last_modified_user = dr["last_modified_user"].ToString();
                category.Creation_date = dr["creation_date"].ToString();
                category.Creation_user = dr["creation_user"].ToString();
                category.Category_id = int.Parse(dr["Category_id"].ToString());
                category.Category_name = dr["Category_name"].ToString();
                category.Status = int.Parse(dr["status"].ToString());
            }
            return category;
        }

        public Category GetCategoryByID(int id) {
            Category category = new Category();
            DataTable table = categoryDAO.SearchByID(id);
            foreach (DataRow dr in table.Rows) {
                category.Last_modified = dr["last_modified"].ToString();
                category.Last_modified_user = dr["last_modified_user"].ToString();
                category.Creation_date = dr["creation_date"].ToString();
                category.Creation_user = dr["creation_user"].ToString();
                category.Category_id = int.Parse(dr["Category_id"].ToString());
                category.Category_name = dr["Category_name"].ToString();
                category.Status = int.Parse(dr["status"].ToString());
            }
            return category;
        }

        public List<Category> GetCategories() {
            List<Category> cat = new List<Category>();
            DataTable table = categoryDAO.GetAll();
            foreach (DataRow dr in table.Rows) {
                cat.Add(new Category() {
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Category_id = int.Parse(dr["Category_id"].ToString()),
                    Category_name = dr["Category_name"].ToString(),
                    Status = int.Parse(dr["status"].ToString())
                });
            }
            return cat;
        }

        internal void DeleteCategory(int v) {
            categoryDAO.Delete(v);
        }

        internal void InsertCategory(string name) {
            categoryDAO.Insert(name);
        }

        internal void EditCategory(int v, string text, int status) {
            categoryDAO.Edit(v, text, status);
        }
    }
}
