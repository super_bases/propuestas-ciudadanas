﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Citizen.Business {
    class CountryBUS {
        private CountryDAO countryDAO;

        public CountryBUS() {
            countryDAO = new CountryDAO();
        }

        public void InsertCountry(string name) {
            countryDAO.Insert(name);
        }

        public void DeleteCountry(int id) {
            countryDAO.Delete(id);
        }

        public Country GetCountryByID(int id) {
            Country country = new Country();
            DataTable table = countryDAO.SearchCountryByID(id);
            foreach (DataRow dr in table.Rows) {
                country.Id = int.Parse(dr["country_id"].ToString());
                country.Name = dr["name"].ToString();
                country.Creation_date = dr["creation_date"].ToString();
                country.Creation_user = dr["creation_user"].ToString();
                country.Last_modified = dr["last_modified"].ToString();
                country.Last_modified_user = dr["last_modified_user"].ToString();
                country.Status = int.Parse(dr["status"].ToString());
            }
            return country;
        }

        public ArrayList getCountries() {
            ArrayList cts = new ArrayList();
            DataTable table = countryDAO.SearchAllCountries();
            foreach (DataRow dr in table.Rows) {
                Country country = new Country {
                    Id = int.Parse(dr["country_id"].ToString()),
                    Name = dr["name"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString(),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Status = int.Parse(dr["status"].ToString())
                };
                cts.Add(country);
            }

            return cts;
        }

        internal void EditCountry(int v, string text, int status) {
            countryDAO.Edit(v, text, status);
        }
    }
}
