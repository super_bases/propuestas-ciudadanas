﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Citizen.Data_Access;

namespace Citizen.Business {
    class UserBUS {
        private UserDAO userDAO;

        public UserBUS() {
            userDAO = new UserDAO();
        }

        public User GetUserByUsername(string username) {
            User user = new User();
            DataTable table = userDAO.SearchByUsername(username);
            foreach(DataRow dr in table.Rows){
                user.Username = dr["username"].ToString();
                user.Password = dr["password"].ToString();
                user.Type_id = int.Parse(dr["type_id"].ToString());
                user.Last_modified = dr["last_modified"].ToString();
                user.Last_modified_user = dr["last_modified_user"].ToString();
                user.Creation_date = dr["creation_date"].ToString();
                user.Creation_user = dr["creation_user"].ToString();
            }
            return user;
        }

        public List<User> GetUsers() {
            List<User> users = new List<User>();
            DataTable table = userDAO.SearchAllUsers();
            foreach(DataRow dr in table.Rows) {
                users.Add(new User() {
                    Username = dr["username"].ToString(),
                    Password = dr["password"].ToString(),
                    Type_id = int.Parse(dr["type_id"].ToString()),
                    Last_modified = dr["last_modified"].ToString(),
                    Last_modified_user = dr["last_modified_user"].ToString(),
                    Creation_date = dr["creation_date"].ToString(),
                    Creation_user = dr["creation_user"].ToString()
                });
            }
            return users;
        }

    }
}