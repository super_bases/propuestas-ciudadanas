﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class Votes_LogBUS {
        private Votes_LogDAO votes_LogDAO;

        public Votes_LogBUS() {
            votes_LogDAO = new Votes_LogDAO();
        }

        public Votes_Log GetVotesByProjectID(int id) {
            Votes_Log vote_log = new Votes_Log();
            DataTable table = votes_LogDAO.SearchByProjectID(id);
            foreach (DataRow dr in table.Rows) {
                vote_log.Last_modified = dr["last_modified"].ToString();
                vote_log.Last_modified_user = dr["last_modified_user"].ToString();
                vote_log.Creation_date = dr["creation_date"].ToString();
                vote_log.Creation_user = dr["creation_user"].ToString();
                vote_log.Current_votes = int.Parse(dr["current_votes"].ToString());
                vote_log.Last_votes = int.Parse(dr["last_votes"].ToString());
                vote_log.Votes_id = int.Parse(dr["last_votes"].ToString());
            }
            return vote_log;
        }
    }
}
