﻿using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Business {
    class UserXFavouriteBUS {
        private UserXFavouriteDAO userXFavouriteDAO;

        public UserXFavouriteBUS() {
            userXFavouriteDAO = new UserXFavouriteDAO();
        }

        public UserXFavourite GetUxFByUsername(string username) {
            UserXFavourite uxf = new UserXFavourite();
            DataTable table = userXFavouriteDAO.SearchByUsername(username);
            foreach (DataRow dr in table.Rows) {
                uxf.Username = dr["username"].ToString();
                uxf.Last_modified = dr["last_modified"].ToString();
                uxf.Last_modified_user = dr["last_modified_user"].ToString();
                uxf.Creation_date = dr["creation_date"].ToString();
                uxf.Creation_user = dr["creation_user"].ToString();
                uxf.Favourite_id = int.Parse(dr["favourite_id"].ToString());
            }
            return uxf;
        }
    }
}
