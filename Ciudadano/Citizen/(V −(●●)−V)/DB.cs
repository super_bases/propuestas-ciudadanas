﻿using Citizen.Business;
using Citizen.Data_Access;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    public sealed class DB {
        private static DBConnection conn;

        public static DBConnection Conn {
            get {
                if(conn == null)
                     conn = new DBConnection();
                return conn;
            }
        }
    }

    internal sealed class LU {
        private static User u;

        public static User User {
            set {
                if (u == null) {
                    u = value;
                }
            }
            get {               
                return u;
            }
        }
    }

    internal sealed class Community {
        private static Forum forum;

        public static Forum Forum {
            get {
                if (forum == null) {
                    Person person = new PersonBUS().GetPersonByUsername(LU.User.Username);

                    forum = new ForumBUS().GetForumByCantonID(new CantonBUS().GetCantonByDistrictID(person.District_id).Canton_id);
                }
                return forum;
            }
        }
    }
}
