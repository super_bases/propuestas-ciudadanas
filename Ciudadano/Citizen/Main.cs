﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using MetroFramework.Controls;
using Citizen.Business;
using System.Diagnostics;

namespace Citizen {

    public partial class Main : MetroForm {
        ProfileMetroPanel profilePanel;
        ProjectMetroPanel projectPanel;
        TopMetroPanel topPanel;
        JobMetroPanel jobPanel;
        StatMetroPanel statPanel;
        CatalogMetroPanel catalogPanel;
        UsersMetroPanel usersPanel;
        Login login;


        public Main(Login login) {
            InitializeComponent();
            InitializeControls();
            Visible = true;
            projectPanel.Visible = true;
            this.login = login;

        }


        private void InitializeControls() {
            profilePanel = new ProfileMetroPanel(ProfileMode.User);
            mainPanel.Controls.Add(profilePanel);

            projectPanel = new ProjectMetroPanel(Community.Forum.Forum_id);
            mainPanel.Controls.Add(projectPanel);

            topPanel = new TopMetroPanel();
            mainPanel.Controls.Add(topPanel);

            statPanel = new StatMetroPanel();
            mainPanel.Controls.Add(statPanel);

            jobPanel = new JobMetroPanel();
            mainPanel.Controls.Add(jobPanel);

            catalogPanel = new CatalogMetroPanel();
            mainPanel.Controls.Add(catalogPanel);

            usersPanel = new UsersMetroPanel();
            mainPanel.Controls.Add(usersPanel);

            foreach (Control control in mainPanel.Controls) {
                if (control is MetroUserControl) {
                    control.Parent = mainPanel;
                    control.Dock = DockStyle.Fill;
                    control.Visible = false;
                }
            }
        }

        private void SetControlsInvisible() {
            foreach (Control control in mainPanel.Controls) {
                if(control is MetroUserControl)
                    control.Visible = false;
            }
        }
        private void BtnProfile_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            profilePanel.Visible = true;
        }

        private void BtnProjects_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            projectPanel.Visible = true;
        }

        private void BtnTop_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            topPanel.Visible = true;
        }

        private void BtnJobs_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            jobPanel.Visible = true;
        }

        private void BtnUsers_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            usersPanel.Visible = true;
        }

        private void BtnCatalogs_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            catalogPanel.Visible = true;
        }

        private void BtnStats_Click(object sender, EventArgs e) {
            SetControlsInvisible();
            statPanel.Visible = true;
        }

        private void BtnAbout_Click(object sender, EventArgs e) {

        }

        private void Main_Closed(object sender, FormClosedEventArgs e) {
            login.Dispose();
            Dispose();
        }
    }
}
