﻿namespace Citizen {
    partial class CatalogMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabControl = new MetroFramework.Controls.MetroTabControl();
            this.countryTab = new MetroFramework.Controls.MetroTabPage();
            this.nationalityTab = new MetroFramework.Controls.MetroTabPage();
            this.topTab = new MetroFramework.Controls.MetroTabPage();
            this.btnSave = new MetroFramework.Controls.MetroTile();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnEdit = new MetroFramework.Controls.MetroButton();
            this.txtValue = new MetroFramework.Controls.MetroTextBox();
            this.provinceTab = new MetroFramework.Controls.MetroTabPage();
            this.cantonTab = new MetroFramework.Controls.MetroTabPage();
            this.districtTab = new MetroFramework.Controls.MetroTabPage();
            this.categoryTab = new MetroFramework.Controls.MetroTabPage();
            this.tabControl.SuspendLayout();
            this.topTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.countryTab);
            this.tabControl.Controls.Add(this.topTab);
            this.tabControl.Controls.Add(this.nationalityTab);
            this.tabControl.Controls.Add(this.provinceTab);
            this.tabControl.Controls.Add(this.cantonTab);
            this.tabControl.Controls.Add(this.districtTab);
            this.tabControl.Controls.Add(this.categoryTab);
            this.tabControl.Location = new System.Drawing.Point(2, 2);
            this.tabControl.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 2;
            this.tabControl.Size = new System.Drawing.Size(646, 509);
            this.tabControl.Style = MetroFramework.MetroColorStyle.Teal;
            this.tabControl.TabIndex = 0;
            this.tabControl.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // countryTab
            // 
            this.countryTab.HorizontalScrollbarBarColor = true;
            this.countryTab.HorizontalScrollbarSize = 8;
            this.countryTab.Location = new System.Drawing.Point(4, 35);
            this.countryTab.Margin = new System.Windows.Forms.Padding(2);
            this.countryTab.Name = "countryTab";
            this.countryTab.Size = new System.Drawing.Size(638, 470);
            this.countryTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.countryTab.TabIndex = 0;
            this.countryTab.Text = "Country";
            this.countryTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.countryTab.VerticalScrollbarBarColor = true;
            this.countryTab.VerticalScrollbarSize = 8;
            // 
            // nationalityTab
            // 
            this.nationalityTab.HorizontalScrollbarBarColor = true;
            this.nationalityTab.HorizontalScrollbarSize = 8;
            this.nationalityTab.Location = new System.Drawing.Point(4, 35);
            this.nationalityTab.Margin = new System.Windows.Forms.Padding(2);
            this.nationalityTab.Name = "nationalityTab";
            this.nationalityTab.Size = new System.Drawing.Size(638, 470);
            this.nationalityTab.TabIndex = 6;
            this.nationalityTab.Text = "Nationality";
            this.nationalityTab.VerticalScrollbarBarColor = true;
            this.nationalityTab.VerticalScrollbarSize = 8;
            // 
            // topTab
            // 
            this.topTab.Controls.Add(this.btnSave);
            this.topTab.Controls.Add(this.btnCancel);
            this.topTab.Controls.Add(this.btnEdit);
            this.topTab.Controls.Add(this.txtValue);
            this.topTab.HorizontalScrollbarBarColor = true;
            this.topTab.HorizontalScrollbarSize = 8;
            this.topTab.Location = new System.Drawing.Point(4, 35);
            this.topTab.Margin = new System.Windows.Forms.Padding(2);
            this.topTab.Name = "topTab";
            this.topTab.Size = new System.Drawing.Size(638, 470);
            this.topTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.topTab.TabIndex = 4;
            this.topTab.Text = "Top";
            this.topTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.topTab.VerticalScrollbarBarColor = true;
            this.topTab.VerticalScrollbarSize = 8;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(442, 107);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(155, 32);
            this.btnSave.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnSave.TileImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(283, 107);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(155, 32);
            this.btnCancel.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(123, 107);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(155, 32);
            this.btnEdit.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // txtValue
            // 
            this.txtValue.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtValue.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtValue.Location = new System.Drawing.Point(44, 107);
            this.txtValue.Margin = new System.Windows.Forms.Padding(2);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(75, 32);
            this.txtValue.Style = MetroFramework.MetroColorStyle.Teal;
            this.txtValue.TabIndex = 2;
            this.txtValue.Text = "10";
            this.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValue.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtValue.Leave += new System.EventHandler(this.Value_Leave);
            // 
            // provinceTab
            // 
            this.provinceTab.HorizontalScrollbarBarColor = true;
            this.provinceTab.HorizontalScrollbarSize = 8;
            this.provinceTab.Location = new System.Drawing.Point(4, 35);
            this.provinceTab.Margin = new System.Windows.Forms.Padding(2);
            this.provinceTab.Name = "provinceTab";
            this.provinceTab.Size = new System.Drawing.Size(638, 470);
            this.provinceTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.provinceTab.TabIndex = 1;
            this.provinceTab.Text = "Province";
            this.provinceTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.provinceTab.VerticalScrollbarBarColor = true;
            this.provinceTab.VerticalScrollbarSize = 8;
            // 
            // cantonTab
            // 
            this.cantonTab.HorizontalScrollbarBarColor = true;
            this.cantonTab.HorizontalScrollbarSize = 8;
            this.cantonTab.Location = new System.Drawing.Point(4, 35);
            this.cantonTab.Margin = new System.Windows.Forms.Padding(2);
            this.cantonTab.Name = "cantonTab";
            this.cantonTab.Size = new System.Drawing.Size(638, 470);
            this.cantonTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.cantonTab.TabIndex = 2;
            this.cantonTab.Text = "Canton";
            this.cantonTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cantonTab.VerticalScrollbarBarColor = true;
            this.cantonTab.VerticalScrollbarSize = 8;
            // 
            // districtTab
            // 
            this.districtTab.HorizontalScrollbarBarColor = true;
            this.districtTab.HorizontalScrollbarSize = 8;
            this.districtTab.Location = new System.Drawing.Point(4, 35);
            this.districtTab.Margin = new System.Windows.Forms.Padding(2);
            this.districtTab.Name = "districtTab";
            this.districtTab.Size = new System.Drawing.Size(638, 470);
            this.districtTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.districtTab.TabIndex = 3;
            this.districtTab.Text = "District";
            this.districtTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.districtTab.VerticalScrollbarBarColor = true;
            this.districtTab.VerticalScrollbarSize = 8;
            // 
            // categoryTab
            // 
            this.categoryTab.HorizontalScrollbarBarColor = true;
            this.categoryTab.HorizontalScrollbarSize = 8;
            this.categoryTab.Location = new System.Drawing.Point(4, 35);
            this.categoryTab.Margin = new System.Windows.Forms.Padding(2);
            this.categoryTab.Name = "categoryTab";
            this.categoryTab.Size = new System.Drawing.Size(638, 470);
            this.categoryTab.Style = MetroFramework.MetroColorStyle.Teal;
            this.categoryTab.TabIndex = 5;
            this.categoryTab.Text = "Category";
            this.categoryTab.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.categoryTab.VerticalScrollbarBarColor = true;
            this.categoryTab.VerticalScrollbarSize = 8;
            // 
            // CatalogMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Name = "CatalogMetroPanel";
            this.Size = new System.Drawing.Size(651, 514);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tabControl.ResumeLayout(false);
            this.topTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl tabControl;
        private MetroFramework.Controls.MetroTabPage countryTab;
        private MetroFramework.Controls.MetroTabPage provinceTab;
        private MetroFramework.Controls.MetroTabPage cantonTab;
        private MetroFramework.Controls.MetroTabPage districtTab;
        private MetroFramework.Controls.MetroTabPage categoryTab;
        private MetroFramework.Controls.MetroTabPage topTab;
        private MetroFramework.Controls.MetroTile btnSave;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroButton btnEdit;
        private MetroFramework.Controls.MetroTextBox txtValue;
        private MetroFramework.Controls.MetroTabPage nationalityTab;
    }
}
