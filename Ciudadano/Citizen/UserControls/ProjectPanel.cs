﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Diagnostics;
using Citizen.Business;
using Citizen.Data_Access;

namespace Citizen {
    public partial class ProjectMetroPanel : MetroUserControl {

        int forum_id;
        public ProjectMetroPanel(int forum_id) {
            InitializeComponent();
            this.forum_id = forum_id;
            // combo box
            comboCategory.Items.Insert(0, "---");
            List<Category> cat = new CategoryBUS().GetCategories();
            foreach (Category c in cat) {
                comboCategory.Items.Add(c.Category_name);
            }

            Fill(new ProjectBUS().GetProjectsByForumID(forum_id));
        }

        internal void Fill(List<Project> projects) {
            // project list
            projectList.Controls.Clear();
            foreach (Project project in projects) {
                projectList.Controls.Add(new ProjectItem(this, project.Project_id));
            }
            lblTotal.Text = projectList.Controls.Count.ToString();
        }

        private void BtnAll_Click(object sender, EventArgs e) {
            if (btnAll.Text == "See ALL") {
                Fill(new ProjectBUS().GetProjectsByForumID(forum_id));
                btnAll.Text = "See MINE";
            } else if (btnAll.Text == "See MINE") {
                Fill(new ProjectBUS().GetProjectsByUsername(LU.User.Username));
                btnAll.Text = "See ALL";
            }
        }

        private void BtnProposal_Click(object sender, EventArgs e) {
            NewProject project = new NewProject(this) {
                Parent = Parent,
                Visible = true
            };
            Parent.Controls.Add(project);
            Visible = false;
        }

        private void ComboCategory_SelectedIndexChanged(object sender, EventArgs e) { // se necesita select forum_id y category
            if (comboCategory.SelectedItem != null && comboCategory.SelectedItem.ToString() != "---") {
                Fill(new ProjectBUS().GetProjectsByCategoryName(comboCategory.SelectedItem.ToString()));
            } else  {
                Fill(new ProjectBUS().GetProjectsByForumID(Community.Forum.Forum_id));
            }

        }

        private void EndDate_ValueChanged(object sender, EventArgs e) {
            if (endDatePicker.Checked && startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(startDatePicker.Value.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("start and end: {0} {1}", startDatePicker.Value.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));

            } else if (!endDatePicker.Checked && startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(startDatePicker.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("start: {0} {1}", startDatePicker.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy")));

            } else if (endDatePicker.Checked && !startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(DateTime.MinValue.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("end: {0} {1}", DateTime.MinValue.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
            } else {
                Fill(new ProjectBUS().GetProjectsByForumID(Community.Forum.Forum_id));
            }
        }

        private void StartDate_ValueChanged(object sender, EventArgs e) {
            if (endDatePicker.Checked && startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(startDatePicker.Value.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("start and end: {0} {1}", startDatePicker.Value.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));

            } else if (!endDatePicker.Checked && startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(startDatePicker.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("start: {0} {1}", startDatePicker.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy")));

            } else if (endDatePicker.Checked && !startDatePicker.Checked) {
                Fill(new ProjectBUS().GetProjectsInDateRange(DateTime.MinValue.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
                Debug.WriteLine(string.Format("end: {0} {1}", DateTime.MinValue.ToString("dd/MM/yyyy"), endDatePicker.Value.ToString("dd/MM/yyyy")));
            } else {
                Fill(new ProjectBUS().GetProjectsByForumID(Community.Forum.Forum_id));
            }
        }
    }
}

