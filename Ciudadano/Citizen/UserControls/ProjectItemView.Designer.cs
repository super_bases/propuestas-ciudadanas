﻿namespace Citizen {
    partial class ProjectItemView {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblProjectName = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblProjectCreator = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.lblBudget = new MetroFramework.Controls.MetroLabel();
            this.btnComment = new MetroFramework.Controls.MetroButton();
            this.commentList = new System.Windows.Forms.FlowLayoutPanel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblVotes = new MetroFramework.Controls.MetroLabel();
            this.btnVote = new MetroFramework.Controls.MetroTile();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.btnBack = new MetroFramework.Controls.MetroTile();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblCategory = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // lblProjectName
            // 
            this.lblProjectName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblProjectName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblProjectName.Location = new System.Drawing.Point(0, 0);
            this.lblProjectName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProjectName.MaximumSize = new System.Drawing.Size(525, 24);
            this.lblProjectName.MinimumSize = new System.Drawing.Size(525, 24);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(525, 24);
            this.lblProjectName.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProjectName.TabIndex = 0;
            this.lblProjectName.Text = "Project Name";
            this.lblProjectName.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(2, 26);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(23, 19);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "by";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblProjectCreator
            // 
            this.lblProjectCreator.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblProjectCreator.Location = new System.Drawing.Point(25, 26);
            this.lblProjectCreator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProjectCreator.MaximumSize = new System.Drawing.Size(450, 16);
            this.lblProjectCreator.MinimumSize = new System.Drawing.Size(450, 16);
            this.lblProjectCreator.Name = "lblProjectCreator";
            this.lblProjectCreator.Size = new System.Drawing.Size(450, 16);
            this.lblProjectCreator.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProjectCreator.TabIndex = 2;
            this.lblProjectCreator.Text = "Project Creator on Date 00/00/0000";
            this.lblProjectCreator.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(2, 150);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(54, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Budget:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblBudget
            // 
            this.lblBudget.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblBudget.Location = new System.Drawing.Point(55, 150);
            this.lblBudget.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBudget.MaximumSize = new System.Drawing.Size(225, 16);
            this.lblBudget.MinimumSize = new System.Drawing.Size(225, 16);
            this.lblBudget.Name = "lblBudget";
            this.lblBudget.Size = new System.Drawing.Size(225, 16);
            this.lblBudget.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblBudget.TabIndex = 5;
            this.lblBudget.Text = "$1000000000000000000000000";
            this.lblBudget.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnComment
            // 
            this.btnComment.Location = new System.Drawing.Point(512, 178);
            this.btnComment.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnComment.Name = "btnComment";
            this.btnComment.Size = new System.Drawing.Size(137, 19);
            this.btnComment.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnComment.TabIndex = 6;
            this.btnComment.Text = "Comment";
            this.btnComment.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnComment.Click += new System.EventHandler(this.BtnComment_Click);
            // 
            // commentList
            // 
            this.commentList.AutoScroll = true;
            this.commentList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.commentList.Location = new System.Drawing.Point(2, 206);
            this.commentList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.commentList.Name = "commentList";
            this.commentList.Size = new System.Drawing.Size(645, 306);
            this.commentList.TabIndex = 7;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(2, 180);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(43, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "Votes:";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblVotes
            // 
            this.lblVotes.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblVotes.Location = new System.Drawing.Point(41, 180);
            this.lblVotes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVotes.MaximumSize = new System.Drawing.Size(225, 16);
            this.lblVotes.MinimumSize = new System.Drawing.Size(225, 16);
            this.lblVotes.Name = "lblVotes";
            this.lblVotes.Size = new System.Drawing.Size(225, 16);
            this.lblVotes.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblVotes.TabIndex = 9;
            this.lblVotes.Text = "1000000000000000000000000";
            this.lblVotes.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnVote
            // 
            this.btnVote.Location = new System.Drawing.Point(451, 178);
            this.btnVote.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnVote.Name = "btnVote";
            this.btnVote.Size = new System.Drawing.Size(56, 19);
            this.btnVote.Style = MetroFramework.MetroColorStyle.Green;
            this.btnVote.TabIndex = 10;
            this.btnVote.Text = "Vote";
            this.btnVote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVote.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnVote.Click += new System.EventHandler(this.BtnVote_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Enabled = false;
            this.txtDescription.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtDescription.Location = new System.Drawing.Point(2, 50);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(646, 89);
            this.txtDescription.TabIndex = 11;
            this.txtDescription.Text = "description";
            this.txtDescription.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(555, 2);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(92, 34);
            this.btnBack.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnBack.TabIndex = 12;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnBack.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(451, 150);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(67, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "Category:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCategory
            // 
            this.lblCategory.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblCategory.Location = new System.Drawing.Point(512, 150);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCategory.MaximumSize = new System.Drawing.Size(135, 16);
            this.lblCategory.MinimumSize = new System.Drawing.Size(135, 16);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(135, 16);
            this.lblCategory.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblCategory.TabIndex = 14;
            this.lblCategory.Text = "Category Name";
            this.lblCategory.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // ProjectItemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.btnVote);
            this.Controls.Add(this.lblVotes);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.commentList);
            this.Controls.Add(this.btnComment);
            this.Controls.Add(this.lblBudget);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.lblProjectCreator);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.lblProjectName);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ProjectItemView";
            this.Size = new System.Drawing.Size(651, 514);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblProjectName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblProjectCreator;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblBudget;
        private MetroFramework.Controls.MetroButton btnComment;
        private System.Windows.Forms.FlowLayoutPanel commentList;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblVotes;
        private MetroFramework.Controls.MetroTile btnVote;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroTile btnBack;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblCategory;
    }
}
