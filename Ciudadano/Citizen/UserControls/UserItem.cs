﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Diagnostics;
using Citizen.Data_Access;
using Citizen.Business;

namespace Citizen {
    internal partial class UserItem : MetroUserControl {

        private User user;
        private Person person;

        public User User { get => user; }
        public Person Person { get => person; }
        public UserItem(User user) {
            InitializeComponent();
            this.user = user;
            person = new PersonBUS().GetPersonByUsername(user.Username);

            DataTable canton_data = new CantonDAO().SearchCantonByDistrictID(person.District_id);
            DataRow row2 = canton_data.Rows[0];
            int canton = int.Parse(row2["canton_id"].ToString());

            DataTable forum_data = new PersonDAO().SearchForumByCantonID(canton);
            DataRow row = forum_data.Rows[0];
            string foro = row["forum_name"].ToString();
            //////
            lblCommunity.Text = foro;
            //lblCommunity.Text = Community.Forum.Forum_name;
            lblFullName.Text = string.Format("{0} {1} {2}", person.Name, person.First_lastname, person.Second_lastname);
            lblUsername.Text = user.Username;
            // dias desde la ultimo cambio de constrasena lblDays.Text =
        }

        private void BtnView_Click(object sender, EventArgs e) {
            ProfileMetroPanel profile = new ProfileMetroPanel(ProfileMode.Admin, this) {
                Parent = Parent.Parent.Parent,
                Visible = true
            };
            Parent.Parent.Parent.Controls.Add(profile);
            Parent.Parent.Visible = false;
        }
    }
}
