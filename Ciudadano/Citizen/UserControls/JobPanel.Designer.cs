﻿namespace Citizen {
    partial class JobMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JobMetroPanel));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblProposalName = new MetroFramework.Controls.MetroLabel();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.lblCategory = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lvlVotes = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblBudget = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.lblProjectCreator = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(24, 29);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(267, 25);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Today\'s most voted proposal";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblProposalName
            // 
            this.lblProposalName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblProposalName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblProposalName.Location = new System.Drawing.Point(24, 69);
            this.lblProposalName.MaximumSize = new System.Drawing.Size(600, 30);
            this.lblProposalName.MinimumSize = new System.Drawing.Size(600, 30);
            this.lblProposalName.Name = "lblProposalName";
            this.lblProposalName.Size = new System.Drawing.Size(600, 30);
            this.lblProposalName.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProposalName.TabIndex = 2;
            this.lblProposalName.Text = "Proposal name";
            this.lblProposalName.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.txtDescription.Location = new System.Drawing.Point(24, 133);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(815, 300);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.Text = resources.GetString("txtDescription.Text");
            this.txtDescription.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCategory
            // 
            this.lblCategory.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblCategory.Location = new System.Drawing.Point(105, 522);
            this.lblCategory.MaximumSize = new System.Drawing.Size(180, 20);
            this.lblCategory.MinimumSize = new System.Drawing.Size(180, 20);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(180, 20);
            this.lblCategory.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblCategory.TabIndex = 22;
            this.lblCategory.Text = "Category Name";
            this.lblCategory.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(24, 522);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(69, 20);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel2.TabIndex = 21;
            this.metroLabel2.Text = "Category:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lvlVotes
            // 
            this.lvlVotes.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lvlVotes.Location = new System.Drawing.Point(76, 483);
            this.lvlVotes.MaximumSize = new System.Drawing.Size(300, 20);
            this.lvlVotes.MinimumSize = new System.Drawing.Size(300, 20);
            this.lvlVotes.Name = "lvlVotes";
            this.lvlVotes.Size = new System.Drawing.Size(300, 20);
            this.lvlVotes.Style = MetroFramework.MetroColorStyle.Teal;
            this.lvlVotes.TabIndex = 20;
            this.lvlVotes.Text = "1000000000000000000000000";
            this.lvlVotes.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(24, 483);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(46, 20);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel5.TabIndex = 19;
            this.metroLabel5.Text = "Votes:";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblBudget
            // 
            this.lblBudget.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblBudget.Location = new System.Drawing.Point(94, 446);
            this.lblBudget.MaximumSize = new System.Drawing.Size(300, 20);
            this.lblBudget.MinimumSize = new System.Drawing.Size(300, 20);
            this.lblBudget.Name = "lblBudget";
            this.lblBudget.Size = new System.Drawing.Size(300, 20);
            this.lblBudget.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblBudget.TabIndex = 18;
            this.lblBudget.Text = "$1000000000000000000000000";
            this.lblBudget.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(24, 446);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(56, 20);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel3.TabIndex = 17;
            this.metroLabel3.Text = "Budget:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblProjectCreator
            // 
            this.lblProjectCreator.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblProjectCreator.Location = new System.Drawing.Point(54, 99);
            this.lblProjectCreator.MaximumSize = new System.Drawing.Size(600, 20);
            this.lblProjectCreator.MinimumSize = new System.Drawing.Size(600, 20);
            this.lblProjectCreator.Name = "lblProjectCreator";
            this.lblProjectCreator.Size = new System.Drawing.Size(600, 20);
            this.lblProjectCreator.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProjectCreator.TabIndex = 16;
            this.lblProjectCreator.Text = "Project Creator on Date 00/00/0000";
            this.lblProjectCreator.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(24, 99);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(24, 20);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel6.TabIndex = 15;
            this.metroLabel6.Text = "by";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // JobMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.lvlVotes);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.lblBudget);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.lblProjectCreator);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblProposalName);
            this.Controls.Add(this.metroLabel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "JobMetroPanel";
            this.Size = new System.Drawing.Size(868, 633);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblProposalName;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroLabel lblCategory;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lvlVotes;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblBudget;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblProjectCreator;
        private MetroFramework.Controls.MetroLabel metroLabel6;
    }
}
