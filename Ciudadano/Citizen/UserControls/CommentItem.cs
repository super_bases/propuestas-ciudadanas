﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Citizen {
    public partial class CommentItem : MetroUserControl {

        public string Username { get => lblUsername.Text; set => lblUsername.Text = value;  }
        public string Date { get => lblDate.Text; set => lblDate.Text = value; }

        public string Comment { get => txtText.Text; set => txtText.Text = value; }

        ProjectItem parent;
        public CommentItem(ProjectItem parent) {
            InitializeComponent();
            this.parent = parent;
            parent.Parent.Parent.Visible = false;
        }
    }
}
