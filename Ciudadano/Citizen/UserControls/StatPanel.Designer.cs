﻿namespace Citizen {
    partial class StatMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pieChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnCategory = new MetroFramework.Controls.MetroTile();
            this.btnCountryP = new MetroFramework.Controls.MetroTile();
            this.btnProvinceP = new MetroFramework.Controls.MetroTile();
            this.btnCantonP = new MetroFramework.Controls.MetroTile();
            this.btnDistrictU = new MetroFramework.Controls.MetroTile();
            this.btnCantonU = new MetroFramework.Controls.MetroTile();
            this.btnProvinceU = new MetroFramework.Controls.MetroTile();
            this.btnCountryU = new MetroFramework.Controls.MetroTile();
            this.btnAge = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.pieChart)).BeginInit();
            this.SuspendLayout();
            // 
            // pieChart
            // 
            this.pieChart.BackColor = System.Drawing.Color.Transparent;
            this.pieChart.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea9.Name = "ChartArea1";
            this.pieChart.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.pieChart.Legends.Add(legend9);
            this.pieChart.Location = new System.Drawing.Point(22, 20);
            this.pieChart.Name = "pieChart";
            this.pieChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series9.Legend = "Legend1";
            series9.Name = "pieChart1";
            this.pieChart.Series.Add(series9);
            this.pieChart.Size = new System.Drawing.Size(568, 452);
            this.pieChart.TabIndex = 0;
            this.pieChart.Text = "chart1";
            // 
            // btnCategory
            // 
            this.btnCategory.Location = new System.Drawing.Point(22, 489);
            this.btnCategory.Margin = new System.Windows.Forms.Padding(5);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Size = new System.Drawing.Size(120, 120);
            this.btnCategory.TabIndex = 1;
            this.btnCategory.Text = "Category";
            this.btnCategory.Click += new System.EventHandler(this.BtnCategory_Click);
            // 
            // btnCountryP
            // 
            this.btnCountryP.Location = new System.Drawing.Point(150, 489);
            this.btnCountryP.Margin = new System.Windows.Forms.Padding(5);
            this.btnCountryP.Name = "btnCountryP";
            this.btnCountryP.Size = new System.Drawing.Size(92, 55);
            this.btnCountryP.TabIndex = 4;
            this.btnCountryP.Text = "Country";
            this.btnCountryP.Click += new System.EventHandler(this.BtnCountryP_Click);
            // 
            // btnProvinceP
            // 
            this.btnProvinceP.Location = new System.Drawing.Point(150, 554);
            this.btnProvinceP.Margin = new System.Windows.Forms.Padding(5);
            this.btnProvinceP.Name = "btnProvinceP";
            this.btnProvinceP.Size = new System.Drawing.Size(92, 55);
            this.btnProvinceP.TabIndex = 5;
            this.btnProvinceP.Text = "Province";
            this.btnProvinceP.Click += new System.EventHandler(this.BtnProvinceP_Click);
            // 
            // btnCantonP
            // 
            this.btnCantonP.Location = new System.Drawing.Point(252, 489);
            this.btnCantonP.Margin = new System.Windows.Forms.Padding(5);
            this.btnCantonP.Name = "btnCantonP";
            this.btnCantonP.Size = new System.Drawing.Size(92, 55);
            this.btnCantonP.TabIndex = 6;
            this.btnCantonP.Text = "Canton";
            this.btnCantonP.Click += new System.EventHandler(this.BtnCantonP_Click);
            // 
            // btnDistrictU
            // 
            this.btnDistrictU.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnDistrictU.Location = new System.Drawing.Point(623, 554);
            this.btnDistrictU.Margin = new System.Windows.Forms.Padding(5);
            this.btnDistrictU.Name = "btnDistrictU";
            this.btnDistrictU.Size = new System.Drawing.Size(92, 55);
            this.btnDistrictU.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnDistrictU.TabIndex = 12;
            this.btnDistrictU.Text = "District";
            this.btnDistrictU.Click += new System.EventHandler(this.BtnDistrictU_Click);
            // 
            // btnCantonU
            // 
            this.btnCantonU.Location = new System.Drawing.Point(623, 489);
            this.btnCantonU.Margin = new System.Windows.Forms.Padding(5);
            this.btnCantonU.Name = "btnCantonU";
            this.btnCantonU.Size = new System.Drawing.Size(92, 55);
            this.btnCantonU.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnCantonU.TabIndex = 11;
            this.btnCantonU.Text = "Canton";
            this.btnCantonU.Click += new System.EventHandler(this.BtnCantonU_Click);
            // 
            // btnProvinceU
            // 
            this.btnProvinceU.Location = new System.Drawing.Point(521, 554);
            this.btnProvinceU.Margin = new System.Windows.Forms.Padding(5);
            this.btnProvinceU.Name = "btnProvinceU";
            this.btnProvinceU.Size = new System.Drawing.Size(92, 55);
            this.btnProvinceU.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnProvinceU.TabIndex = 10;
            this.btnProvinceU.Text = "Province";
            this.btnProvinceU.Click += new System.EventHandler(this.BtnProvinceU_Click);
            // 
            // btnCountryU
            // 
            this.btnCountryU.Location = new System.Drawing.Point(521, 489);
            this.btnCountryU.Margin = new System.Windows.Forms.Padding(5);
            this.btnCountryU.Name = "btnCountryU";
            this.btnCountryU.Size = new System.Drawing.Size(92, 55);
            this.btnCountryU.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnCountryU.TabIndex = 9;
            this.btnCountryU.Text = "Country";
            this.btnCountryU.Click += new System.EventHandler(this.BtnCountryU_Click);
            // 
            // btnAge
            // 
            this.btnAge.Location = new System.Drawing.Point(725, 489);
            this.btnAge.Margin = new System.Windows.Forms.Padding(5);
            this.btnAge.Name = "btnAge";
            this.btnAge.Size = new System.Drawing.Size(120, 120);
            this.btnAge.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnAge.TabIndex = 8;
            this.btnAge.Text = "Age";
            this.btnAge.Click += new System.EventHandler(this.BtnAge_Click);
            // 
            // StatMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDistrictU);
            this.Controls.Add(this.btnCantonU);
            this.Controls.Add(this.btnProvinceU);
            this.Controls.Add(this.btnCountryU);
            this.Controls.Add(this.btnAge);
            this.Controls.Add(this.btnCantonP);
            this.Controls.Add(this.btnProvinceP);
            this.Controls.Add(this.btnCountryP);
            this.Controls.Add(this.btnCategory);
            this.Controls.Add(this.pieChart);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StatMetroPanel";
            this.Size = new System.Drawing.Size(868, 633);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            ((System.ComponentModel.ISupportInitialize)(this.pieChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart pieChart;
        private MetroFramework.Controls.MetroTile btnCategory;
        private MetroFramework.Controls.MetroTile btnCountryP;
        private MetroFramework.Controls.MetroTile btnProvinceP;
        private MetroFramework.Controls.MetroTile btnCantonP;
        private MetroFramework.Controls.MetroTile btnDistrictU;
        private MetroFramework.Controls.MetroTile btnCantonU;
        private MetroFramework.Controls.MetroTile btnProvinceU;
        private MetroFramework.Controls.MetroTile btnCountryU;
        private MetroFramework.Controls.MetroTile btnAge;
    }
}
