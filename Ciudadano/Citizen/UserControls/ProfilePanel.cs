﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Controls;
using System.Collections;
using Citizen.Business;
using System.Security.Cryptography;

public enum ProfileMode { Admin, New, User }
/*
    Admin es para cuando el admin abre el perfil de cualquier usuario
    User es la vista normal del usuario
    New para el registro
*/

namespace Citizen {
    internal partial class ProfileMetroPanel : MetroUserControl {

        #region catalogs
        readonly PersonBUS person;
        #endregion

        private MetroUserControl previous;
        private List<Control> editables;
        private List<Control> nonEditables;
        private ProfileMode mode;
        public ProfileMetroPanel(ProfileMode mode, MetroUserControl previous = null) {
            InitializeComponent();

            person = new PersonBUS();

            birthDatePicker.MaxDate = DateTime.Today.AddYears(-13);
            editables = new List<Control>();
            nonEditables = new List<Control>();

            foreach (Control control in metroPanel1.Controls) {
                if (control is MetroTextBox || control is MetroLabel || control is DateTimePicker || control is MetroComboBox)
                    nonEditables.Add(control);
            }

            foreach (Control control in metroPanel2.Controls) {
                if (control is MetroTextBox || control is MetroLabel || control is DateTimePicker || control is MetroComboBox)
                    editables.Add(control);
            }


            if (mode == ProfileMode.New) {
                btnEdit.Visible = false;
                ckAdmin.Visible = false;
                btnNext.Visible = true;
                btnSave.Visible = true;
                btnCancel.Visible = false;
                btnEdit.Visible = false;

                DisableHalf();
                //llena el combo de paises
                llenarComboPaises();

                //llena el combo de nacionalidades
                llenarComboNacionalidades();

            } else if (mode == ProfileMode.User) {
                DisableAll();
                ckAdmin.Visible = false;
                btnNext.Visible = false;
                metroLabel2.Text = "Change Password Here";
                InfoUser(LU.User.Username.ToString());

            } else if (mode == ProfileMode.Admin) {
                UserItem user = previous as UserItem;
                DisableAll();
                ckAdmin.Visible = true;
                ckAdmin.Enabled = false;
                btnNext.Visible = false;
                btnEdit.Visible = false;
                btnCancel.Visible = true;
                InfoUser(user.User.Username);
            }

            //DisableEditables();
            //DisableNonEditables();
            this.mode = mode;
            this.previous = previous;

            //llena el combo de paises           

            //llena el combo de nacionalidades

        }

        static string encrypt(string rawData) {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create()) {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++) {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        //private void llenarComboNacionalidadesPERSONA() {
        //    DataTable nationality_data = new Data_Access.PersonDAO().SearchPersonNationality(LU.User.Username);
        //    if (nationality_data.Rows.Count == 1) {
        //        DataRow rowNationality = nationality_data.Rows[0];
        //        DataTable name_data = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality["nationality_id"].ToString()));
        //        //DataRow rowName = name_data.Rows[0];
        //        comboNationality1.DataSource = name_data;
        //        comboNationality1.DisplayMember = "nationality_name";
        //        comboNationality1.ValueMember = "nationality_id";
        //    } else {
        //        DataRow rowNationality1 = nationality_data.Rows[0];
        //        DataTable name_data1 = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality1["nationality_id"].ToString()));
        //        DataRow rowNationality2 = nationality_data.Rows[1];
        //        DataTable name_data2 = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality2["nationality_id"].ToString()));
        //        comboNationality1.DataSource = name_data1;
        //        comboNationality1.DisplayMember = "nationality_name";
        //        comboNationality1.ValueMember = "nationality_id";
        //        comboNationality2.DataSource = name_data2;
        //        comboNationality2.DisplayMember = "nationality_name";
        //        comboNationality2.ValueMember = "nationality_id";
        //    }
        //}

        private void llenarComboNacionalidadesVerUser(string username) {
            DataTable nationality_data = new Data_Access.PersonDAO().SearchPersonNationality(username);
            if (nationality_data.Rows.Count == 1) {
                DataRow rowNationality = nationality_data.Rows[0];
                DataTable name_data = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality["nationality_id"].ToString()));
                //DataRow rowName = name_data.Rows[0];
                comboNationality1.DataSource = name_data;
                comboNationality1.DisplayMember = "nationality_name";
                comboNationality1.ValueMember = "nationality_id";
            } else {
                DataRow rowNationality1 = nationality_data.Rows[0];
                DataTable name_data1 = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality1["nationality_id"].ToString()));
                DataRow rowNationality2 = nationality_data.Rows[1];
                DataTable name_data2 = new Data_Access.NationalityDAO().SearchNationalityByID(int.Parse(rowNationality2["nationality_id"].ToString()));
                comboNationality1.DataSource = name_data1;
                comboNationality1.DisplayMember = "nationality_name";
                comboNationality1.ValueMember = "nationality_id";
                comboNationality2.DataSource = name_data2;
                comboNationality2.DisplayMember = "nationality_name";
                comboNationality2.ValueMember = "nationality_id";
            }
        }

        private void llenarComboPaises() {
            DataTable country_data = new Data_Access.CountryDAO().SearchAllCountries();
            comboCountry.DataSource = country_data;
            comboCountry.DisplayMember = "name";
            comboCountry.ValueMember = "country_id";
        }

        private void llenarCombosAddressPERSONA(int id) {
            
            DataTable canton_data = new Data_Access.CantonDAO().SearchCantonByDistrictID(id);
            DataRow rowCanton = canton_data.Rows[0];
            Console.WriteLine("Canton: " + rowCanton["canton_id"].ToString());

            DataTable province_data = new Data_Access.ProvinceDAO().SearchProvinceByCantonID(int.Parse(rowCanton["canton_id"].ToString()));
            DataRow rowProvince = province_data.Rows[0];
            Console.WriteLine("Province: " + rowProvince["province_id"].ToString());

            DataTable country_data = new Data_Access.CountryDAO().SearchCountryByProvinceID(int.Parse(rowProvince["province_id"].ToString()));
            DataRow rowCountry = country_data.Rows[0];
            Console.WriteLine("Country: " + rowCountry["country_id"].ToString());

            DataTable district_data = new Data_Access.DistrictDAO().SearchDistrictByCantonID(int.Parse(rowCanton["canton_id"].ToString()));
            DataRow rowDistrict = district_data.Rows[0];
            Console.WriteLine("District: " + rowDistrict["district_id"].ToString());

            llenarComboPaises();
            comboCountry.SelectedValue = int.Parse(rowCountry["country_id"].ToString());
            comboProvince.SelectedValue = int.Parse(rowProvince["province_id"].ToString());
            comboCanton.SelectedValue = int.Parse(rowCanton["canton_id"].ToString());
            comboDistrict.SelectedValue = int.Parse(rowDistrict["district_id"].ToString());
            //comboCanton.DataSource = canton_data;
            //comboCanton.DisplayMember = "name";
            //comboCanton.ValueMember = "canton_id";
            //comboDistrict.DataSource = district_data;
            //comboDistrict.DisplayMember = "name";
            //comboDistrict.ValueMember = "district_id";
            //comboProvince.DataSource = province_data;
            //comboProvince.DisplayMember = "name";
            //comboProvince.ValueMember = "province_id";
            //comboCountry.DataSource = country_data;
            //comboCountry.DisplayMember = "name";
            //comboCountry.ValueMember = "country_id";




        }

        private void llenarComboNacionalidades() {
            DataTable nationalities_data = new Data_Access.NationalityDAO().SearchNationalities();
            comboNationality1.DataSource = nationalities_data;
            comboNationality1.DisplayMember = "nationality_name";
            comboNationality1.ValueMember = "nationality_id";
        }

        private void llenarComboNacionalidades2() {
            DataTable nationalities_data = new Data_Access.NationalityDAO().SearchNationalities();
            comboNationality2.DataSource = nationalities_data;
            comboNationality2.DisplayMember = "nationality_name";
            comboNationality2.ValueMember = "nationality_id";
        }

        private string getTelefonoPorPersonaID(string param) {
            DataTable phone_data = new Data_Access.PhoneNumberDAO().SearchPhoneByUser(param);
            DataRow row = phone_data.Rows[0];
            return row["phonenumber"].ToString();
        }

        private string getEmailPorPersonaID(string param) {
            DataTable email_data = new Data_Access.EmailDAO().SearchEmailByPersonID(param);
            DataRow row = email_data.Rows[0];
            return row["email"].ToString();
        }

        private void DisableHalf() {
            bool flag = false;
            //txtUsername.Enabled = flag;
            //txtPassword.Enabled = flag;
            txtEmail.Enabled = flag;
            txtName.Enabled = flag;
            txtLastName1.Enabled = flag;
            txtLastName2.Enabled = flag;
            txtID.Enabled = flag;
            txtPhone.Enabled = flag;
            birthDatePicker.Enabled = flag;
            comboNationality1.Enabled = flag;
            comboNationality2.Enabled = flag;
            comboCountry.Enabled = flag;
            comboProvince.Enabled = flag;
            comboCanton.Enabled = flag;
            comboDistrict.Enabled = flag;
            btnSave.Enabled = flag;
        }

        private void EnableInsert() {
            bool flag = true;
            txtEmail.Enabled = flag;
            txtName.Enabled = flag;
            txtLastName1.Enabled = flag;
            txtLastName2.Enabled = flag;
            txtID.Enabled = flag;
            txtPhone.Enabled = flag;
            birthDatePicker.Enabled = flag;
            comboNationality1.Enabled = flag;
            comboNationality2.Enabled = flag;
            comboCountry.Enabled = flag;
            comboProvince.Enabled = flag;
            comboCanton.Enabled = flag;
            comboDistrict.Enabled = flag;
            btnSave.Enabled = flag;
            txtUsername.Enabled = false;
            btnNext.Visible = false;
            btnEdit.Visible = false;
        }

        private void DisableAll() {
            bool flag = false;
            txtUsername.Enabled = flag;
            txtPassword.Enabled = flag;
            txtEmail.Enabled = flag;
            txtName.Enabled = flag;
            txtLastName1.Enabled = flag;
            txtLastName2.Enabled = flag;
            txtID.Enabled = flag;
            txtPhone.Enabled = flag;
            birthDatePicker.Enabled = flag;
            comboNationality1.Enabled = flag;
            comboNationality2.Enabled = flag;
            comboCountry.Enabled = flag;
            comboProvince.Enabled = flag;
            comboCanton.Enabled = flag;
            comboDistrict.Enabled = flag;
            btnSave.Visible = flag;
            btnCancel.Visible = flag;
            btnEdit.Visible = true;
        }

        private void EnableEditables() {
            bool flag = true;
            txtPassword.Enabled = flag;
            txtEmail.Enabled = flag;
            txtName.Enabled = flag;
            txtLastName1.Enabled = flag;
            txtLastName2.Enabled = flag;
            txtID.Enabled = false;
            txtPhone.Enabled = flag;
            birthDatePicker.Enabled = flag;
            comboNationality1.Enabled = flag;
            comboNationality2.Enabled = flag;
            comboCountry.Enabled = flag;
            comboProvince.Enabled = flag;
            comboCanton.Enabled = flag;
            comboDistrict.Enabled = flag;
            btnSave.Visible = flag;
            btnCancel.Visible = flag;
            btnEdit.Visible = false;
            if (mode == ProfileMode.Admin)
                ckAdmin.Enabled = flag;
        }

        private void DisableEditables() {
            bool flag = false;
            txtPassword.Enabled = flag;
            txtEmail.Enabled = flag;
            txtName.Enabled = flag;
            txtLastName1.Enabled = flag;
            txtLastName2.Enabled = flag;
            txtID.Enabled = flag;
            txtPhone.Enabled = flag;
            birthDatePicker.Enabled = flag;
            comboNationality1.Enabled = flag;
            comboNationality2.Enabled = flag;
            comboCountry.Enabled = flag;
            comboProvince.Enabled = flag;
            comboCanton.Enabled = flag;
            comboDistrict.Enabled = flag;
            btnSave.Visible = flag;
            btnCancel.Visible = flag;
            btnEdit.Visible = true;
        }

        private void InfoUser(string username) {
            DataTable person_data = new Data_Access.PersonDAO().SearchByUsername(username);
            DataRow row = person_data.Rows[0];
            txtUsername.Text = row["username"].ToString();
            //txtPassword.Text = LU.User.Password.ToString();
            txtName.Text = row["name"].ToString();
            txtLastName1.Text = row["first_lastname"].ToString();
            txtLastName2.Text = row["second_lastname"].ToString();
            txtID.Text = row["identification"].ToString();
            llenarCombosAddressPERSONA(int.Parse(row["district_id"].ToString()));
            //comboDistrict.SelectedValue = int.Parse(row["district_id"].ToString());
            //comboDistrict.SelectedIndex = int.Parse(row["district_id"].ToString());
            string datetime = row["birthdate"].ToString();
            DateTime fecha = Convert.ToDateTime(datetime);
            birthDatePicker.Value = new DateTime(fecha.Year, fecha.Month, fecha.Day);
            //llenarComboNacionalidadesPERSONA();
            llenarComboNacionalidadesVerUser(username);
            txtEmail.Text = getEmailPorPersonaID(row["username"].ToString());
            txtPhone.Text = getTelefonoPorPersonaID(row["username"].ToString());
        }

        private bool existeUser(string newUser) {
            DataTable user_data = new Data_Access.UserDAO().SearchAllUsers();
            for (int i = 0; i < user_data.Rows.Count; i++) {
                DataRow row = user_data.Rows[i];
                if (newUser.Equals(row["username"].ToString())) {
                    return true;
                }
            }
            return false;
        }

        private bool existeCedula(string newCedula) {
            DataTable person_data = new Data_Access.PersonDAO().SearchAllPersons();
            for (int i = 0; i < person_data.Rows.Count; i++) {
                DataRow row = person_data.Rows[i];
                if (newCedula.Equals(row["identification"].ToString())) {
                    return true;
                }
            }
            return false;
        }

        public static Boolean isNumeric(string valor) {
            int result;
            return int.TryParse(valor, out result);
        }

        private bool sonCamposValidos() {
            if (txtEmail.Text.Contains("@") && isNumeric(txtPhone.Text)) {
                return true;
            }
            return false;
        }

        private bool hayCamposVacios() {
            if (String.IsNullOrEmpty(txtPassword.Text) || String.IsNullOrEmpty(txtEmail.Text) ||
             String.IsNullOrEmpty(txtName.Text) || String.IsNullOrEmpty(txtLastName1.Text) ||String.IsNullOrEmpty(txtLastName2.Text) ||
            String.IsNullOrEmpty(txtID.Text) || String.IsNullOrEmpty(txtPhone.Text) ){
                return true;
            }
            return false;
        }

        //private void DisableNonEditables() {
        //    foreach (Control control in editables) {
        //        control.Enabled = false;
        //    }
        //}

        //private void EnableNonEditables() {
        //    foreach (Control control in nonEditables) {
        //        control.Enabled = true;
        //    }
        //}

        //private void DisableEditables() {
        //    foreach (Control control in nonEditables) {
        //        control.Enabled = false;
        //    }
        //}

        //private void EnableEditables() {
        //    foreach (Control control in editables) {
        //        control.Enabled = true;
        //    }
        //}

        private void BtnEdit_Click(object sender, EventArgs e) {
            EnableEditables();
        }

        private void BtnCancel_Click(object sender, EventArgs e) {
            // return to old values
            if (mode == ProfileMode.User) {  // vista del perfil del usuario
                InfoUser(LU.User.Username.ToString());
                DisableEditables();
            } else if (mode == ProfileMode.New) {
                Parent.Dispose();
            } else if (mode == ProfileMode.Admin) { // mode 2 vista de otros usarios desde un administrador
                previous.Parent.Parent.Visible = true;
                Dispose();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            // validate, hopefully while editing instead of waiting for save button like an idiot
            // update data
            try {
                if (mode == ProfileMode.User) { // vista del perfil del usuario
                    if (!hayCamposVacios() && sonCamposValidos()) {
                        //if (!existeCedula(txtID.Text)) {
                            DisableAll();
                            DataTable person_data = new Data_Access.PersonDAO().SearchByUsername(LU.User.Username);
                            DataRow rowPerson = person_data.Rows[0];
                            int personID = int.Parse(rowPerson["person_id"].ToString());
                            DataRowView drvDist = comboDistrict.SelectedItem as DataRowView;
                            int dis = int.Parse(drvDist.Row["district_id"].ToString());
                            DataRowView drvNat = comboNationality1.SelectedItem as DataRowView;
                            int nat = int.Parse(drvNat.Row["nationality_id"].ToString());
                            string birthdate = birthDatePicker.Value.Year + "/" + birthDatePicker.Value.Month + "/" + birthDatePicker.Value.Day;
                            DataTable phone_data = new Data_Access.PhoneNumberDAO().SearchPhoneByUser(LU.User.Username);
                            DataRow rowP = phone_data.Rows[0];
                            int pn_id = int.Parse(rowP["phonenumber_id"].ToString());
                            DataTable email_data = new Data_Access.EmailDAO().SearchEmailByPersonID(LU.User.Username);
                            DataRow rowE = email_data.Rows[0];
                            int em_id = int.Parse(rowE["email_id"].ToString());

                            person.UpdatePerson(personID, txtID.Text, txtName.Text, txtLastName1.Text,
                                  txtLastName2.Text, birthdate, "", dis, nat, em_id, txtEmail.Text, pn_id, int.Parse(txtPhone.Text));

                        if (txtPassword.Text != "") {
                            string encriptada = encrypt(txtPassword.Text);
                            person.UpdatePassWord(LU.User.Username, encriptada);
                            txtPassword.Text = "";
                        }

                            InfoUser(LU.User.Username.ToString());
                       // } else {
                          //  ErrorMsg er = new ErrorMsg("Identification duplicated!");
                          //  er.Show();
                       // }
                    } else {
                        ErrorMsg er = new ErrorMsg("Check your values! - Upte");
                        er.Show();
                    }

                } else if (mode == ProfileMode.New) {
                    if (!hayCamposVacios() && sonCamposValidos()) {
                        if (!existeCedula(txtID.Text)) {
                            DisableAll();
                            btnEdit.Visible = false;

                            DataRowView drvDist = comboDistrict.SelectedItem as DataRowView;
                            int dis = int.Parse(drvDist.Row["district_id"].ToString());
                            DataRowView drvNat = comboNationality1.SelectedItem as DataRowView;
                            int nat = int.Parse(drvNat.Row["nationality_id"].ToString());
                            string birthdate = birthDatePicker.Value.Year + "/" + birthDatePicker.Value.Month + "/" + birthDatePicker.Value.Day;

                            #region pruebas
                            //Console.WriteLine(txtID.Text);
                            //Console.WriteLine(txtName.Text);
                            //Console.WriteLine(txtLastName1.Text);
                            //Console.WriteLine(txtLastName2.Text);
                            //Console.WriteLine("D: " + dis.ToString());
                            //Console.WriteLine("N: " + nat.ToString());
                            //Console.WriteLine(txtUsername.Text);
                            //Console.WriteLine("BDate: " + birthDatePicker.Value.Year+"/"+ birthDatePicker.Value.Month+"/"+ birthDatePicker.Value.Day);
                            //Console.WriteLine(txtEmail.Text);
                            //Console.WriteLine(txtPhone.Text);
                            #endregion

                            person.InsertPerson(txtID.Text, txtName.Text, txtLastName1.Text,
                               txtLastName2.Text, birthdate, "", txtUsername.Text, dis, nat, txtEmail.Text, int.Parse(txtPhone.Text));
                            CorrectMsg ms = new CorrectMsg("You can now enjoy the app!");
                            DisableAll();
                            btnEdit.Visible = false;
                            ms.Show();
                            //Dispose();
                        } else {
                            ErrorMsg er = new ErrorMsg("Identification duplicated!");
                            er.Show();
                        }
                    } else {
                        ErrorMsg er = new ErrorMsg("Check your values!");
                        er.Show();
                    }

                } else if (mode == ProfileMode.Admin) { // mode 2 vista de otros usarios desde un administrador
                    previous.Parent.Parent.Visible = true;
                    Dispose();
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
                ErrorMsg er = new ErrorMsg("");
                er.Show();
            }
        }

        private void BtnPasslog_Click(object sender, EventArgs e) {
            PassLogControl diag = new PassLogControl(LU.User.Username) {

            };

            if (diag.ShowDialog() == DialogResult.OK) {

            }
        }

        private void BtnFavs_Click(object sender, EventArgs e) {
            FavControl diag = new FavControl() {

            };

            if (diag.ShowDialog() == DialogResult.OK) {

            }
        }

        private void metroPanel1_Paint(object sender, PaintEventArgs e) {

        }

        private void comboCanton_SelectedIndexChanged(object sender, EventArgs e) {
           // comboDistrict.DataSource = null;
            //if (comboCanton.SelectedItem != null) {
            //    DataRowView drv = comboCanton.SelectedItem as DataRowView;
            //    DataTable district_data = new Data_Access.DistrictDAO().SearchDistrictByCantonID(int.Parse(drv.Row["canton_id"].ToString()));
            //    comboDistrict.DataSource = district_data;
            //    comboDistrict.DisplayMember = "name";
            //    comboDistrict.ValueMember = "district_id";
            //}

        }

        private void comboProvince_SelectedIndexChanged(object sender, EventArgs e) {
            comboDistrict.DataSource = null;
            if (comboProvince.SelectedItem != null) {
                DataRowView drv = comboProvince.SelectedItem as DataRowView;
                DataTable canton_data = new Data_Access.CantonDAO().SearchCantonByProvinceID(int.Parse(drv.Row["province_id"].ToString()));
                comboCanton.DataSource = canton_data;
                comboCanton.DisplayMember = "name";
                comboCanton.ValueMember = "canton_id";
            }

        }

        private void comboCountry_SelectedIndexChanged(object sender, EventArgs e) {
            //comboProvince.Items.Clear();
            comboCanton.DataSource = null;
            comboDistrict.DataSource = null;

            if (comboCountry.SelectedItem != null) {
                DataRowView drv = comboCountry.SelectedItem as DataRowView;
                DataTable province_data = new Data_Access.ProvinceDAO().SearchProvinceByCountryID(int.Parse(drv.Row["country_id"].ToString()));
                comboProvince.DataSource = province_data;
                comboProvince.DisplayMember = "name";
                comboProvince.ValueMember = "province_id";

            }

        }

        private void comboCanton_SelectedIndexChanged_1(object sender, EventArgs e) {
            if (comboCanton.SelectedItem != null) {
                DataRowView drv = comboCanton.SelectedItem as DataRowView;
                DataTable district_data = new Data_Access.DistrictDAO().SearchDistrictByCantonID(int.Parse(drv.Row["canton_id"].ToString()));
                comboDistrict.DataSource = district_data;
                comboDistrict.DisplayMember = "name";
                comboDistrict.ValueMember = "district_id";
            }
        }

        private void btnNext_Click(object sender, EventArgs e) {
            //guardar user en la base de datos
            try {
                if (!String.IsNullOrEmpty(txtPassword.Text) || !String.IsNullOrEmpty(txtUsername.Text)) {
                    if (!existeUser(txtUsername.Text)) {
                        Console.WriteLine("User: " + txtUsername.Text);
                        Console.WriteLine("Pass: " + txtPassword.Text);
                        Console.WriteLine("--------------------------------");
                        CorrectMsg ms = new CorrectMsg("Just one more step!");
                        ms.Show();

                        string pass = encrypt(txtPassword.Text);
                        person.InsertUser(txtUsername.Text, pass, 3);
                        EnableInsert();
                    } else {
                        ErrorMsg er = new ErrorMsg("Username already taken!");
                        er.Show();
                    }

                } else {
                    ErrorMsg er = new ErrorMsg("Check your values!");
                    er.Show();
                }
            } catch (Exception ex) {
                ErrorMsg er = new ErrorMsg("");
                er.Show();
            }
        }
    }
}