﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Diagnostics;
using Citizen.Business;

namespace Citizen {
    public partial class ProjectItem : MetroUserControl {

        private Project project;
        private readonly ProjectMetroPanel parent;
        public ProjectItem(ProjectMetroPanel parent, int project_id) {
            InitializeComponent();
            this.parent = parent;
            project = new ProjectBUS().GetProjectByID(project_id);
            SetTextValues();
        }

        private void SetTextValues() {
            lblCategory.Text = new CategoryBUS().GetCategoryByID(project.Category_id).Category_name;
            lblProjectCreator.Text = project.Username;
            lblProjectName.Text = project.Project_name;
            lblVotes.Text = project.Votes.ToString();
        }

        public string ProjectName { get => lblProjectName.Text; set => lblProjectName.Text = value; }
        public string ProjectCreator { get => lblProjectName.Text; set => lblProjectName.Text = value; }
        public int ProjectVotes { get; set; }
        public int ProjectID { get; set; }
        public string ProjectCategory { get => lblCategory.Text; set => lblCategory.Text = value; }

        private void MetroTile1_Click(object sender, EventArgs e) {
            ProjectItemView item = new ProjectItemView(this, project.Project_id) {
                Parent = parent.Parent,
                Visible = true
            };
            Debug.WriteLine(parent.Parent.Name);
            parent.Parent.Controls.Add(item);
            parent.Visible = false;
        }
    }
}
