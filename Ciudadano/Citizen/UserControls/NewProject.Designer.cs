﻿namespace Citizen {
    partial class NewProject {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtProjectName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.txtBudget = new MetroFramework.Controls.MetroTextBox();
            this.comboCategory = new MetroFramework.Controls.MetroComboBox();
            this.btnSave = new MetroFramework.Controls.MetroTile();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtProjectName
            // 
            this.txtProjectName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtProjectName.Location = new System.Drawing.Point(3, 42);
            this.txtProjectName.MaxLength = 100;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(645, 30);
            this.txtProjectName.TabIndex = 0;
            this.txtProjectName.Text = "Title";
            this.txtProjectName.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtProjectName.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtProjectName.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 10);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(232, 25);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "New Community Proposal";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(3, 87);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(645, 318);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.Text = "Write your description here...";
            this.txtDescription.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtDescription.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtDescription.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // txtBudget
            // 
            this.txtBudget.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtBudget.Location = new System.Drawing.Point(3, 421);
            this.txtBudget.MaxLength = 13;
            this.txtBudget.Name = "txtBudget";
            this.txtBudget.Size = new System.Drawing.Size(374, 30);
            this.txtBudget.TabIndex = 4;
            this.txtBudget.Text = "Budget";
            this.txtBudget.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtBudget.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtBudget.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // comboCategory
            // 
            this.comboCategory.FormattingEnabled = true;
            this.comboCategory.ItemHeight = 23;
            this.comboCategory.Location = new System.Drawing.Point(383, 422);
            this.comboCategory.Name = "comboCategory";
            this.comboCategory.Size = new System.Drawing.Size(265, 29);
            this.comboCategory.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(502, 456);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(146, 52);
            this.btnSave.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(352, 456);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(146, 52);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // NewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.comboCategory);
            this.Controls.Add(this.txtBudget);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtProjectName);
            this.MaximumSize = new System.Drawing.Size(651, 514);
            this.MinimumSize = new System.Drawing.Size(651, 514);
            this.Name = "NewProject";
            this.Size = new System.Drawing.Size(651, 514);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtProjectName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroTextBox txtBudget;
        private MetroFramework.Controls.MetroComboBox comboCategory;
        private MetroFramework.Controls.MetroTile btnSave;
        private MetroFramework.Controls.MetroButton btnCancel;
    }
}
