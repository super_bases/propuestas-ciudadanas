﻿namespace Citizen {
    partial class ProjectMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.endDatePicker = new System.Windows.Forms.DateTimePicker();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.comboCategory = new MetroFramework.Controls.MetroComboBox();
            this.btnAll = new MetroFramework.Controls.MetroButton();
            this.projectList = new System.Windows.Forms.FlowLayoutPanel();
            this.btnProposal = new MetroFramework.Controls.MetroTile();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.lblTotal = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(117, 4);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(81, 20);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Date Range";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // startDatePicker
            // 
            this.startDatePicker.Checked = false;
            this.startDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDatePicker.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.startDatePicker.Location = new System.Drawing.Point(117, 31);
            this.startDatePicker.Margin = new System.Windows.Forms.Padding(4);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.ShowCheckBox = true;
            this.startDatePicker.Size = new System.Drawing.Size(189, 22);
            this.startDatePicker.TabIndex = 4;
            this.startDatePicker.Value = new System.DateTime(2019, 10, 15, 0, 0, 0, 0);
            this.startDatePicker.ValueChanged += new System.EventHandler(this.StartDate_ValueChanged);
            // 
            // endDatePicker
            // 
            this.endDatePicker.Checked = false;
            this.endDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDatePicker.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.endDatePicker.Location = new System.Drawing.Point(117, 63);
            this.endDatePicker.Margin = new System.Windows.Forms.Padding(4);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.ShowCheckBox = true;
            this.endDatePicker.Size = new System.Drawing.Size(189, 22);
            this.endDatePicker.TabIndex = 5;
            this.endDatePicker.Value = new System.DateTime(2019, 10, 15, 0, 0, 0, 0);
            this.endDatePicker.ValueChanged += new System.EventHandler(this.EndDate_ValueChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(341, 4);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(66, 20);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Category";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // comboCategory
            // 
            this.comboCategory.FormattingEnabled = true;
            this.comboCategory.ItemHeight = 24;
            this.comboCategory.Location = new System.Drawing.Point(341, 31);
            this.comboCategory.Margin = new System.Windows.Forms.Padding(4);
            this.comboCategory.Name = "comboCategory";
            this.comboCategory.Size = new System.Drawing.Size(252, 30);
            this.comboCategory.Style = MetroFramework.MetroColorStyle.Teal;
            this.comboCategory.TabIndex = 7;
            this.comboCategory.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.comboCategory.SelectedIndexChanged += new System.EventHandler(this.ComboCategory_SelectedIndexChanged);
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(4, 4);
            this.btnAll.Margin = new System.Windows.Forms.Padding(4);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(100, 84);
            this.btnAll.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnAll.TabIndex = 8;
            this.btnAll.Text = "See MINE";
            this.btnAll.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnAll.Click += new System.EventHandler(this.BtnAll_Click);
            // 
            // projectList
            // 
            this.projectList.AutoScroll = true;
            this.projectList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.projectList.Location = new System.Drawing.Point(4, 95);
            this.projectList.Margin = new System.Windows.Forms.Padding(4);
            this.projectList.Name = "projectList";
            this.projectList.Size = new System.Drawing.Size(860, 479);
            this.projectList.TabIndex = 11;
            // 
            // btnProposal
            // 
            this.btnProposal.Location = new System.Drawing.Point(665, 581);
            this.btnProposal.Margin = new System.Windows.Forms.Padding(4);
            this.btnProposal.Name = "btnProposal";
            this.btnProposal.Size = new System.Drawing.Size(199, 48);
            this.btnProposal.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnProposal.TabIndex = 12;
            this.btnProposal.Text = "New Proposal";
            this.btnProposal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnProposal.Click += new System.EventHandler(this.BtnProposal_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(4, 609);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(40, 20);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel4.TabIndex = 13;
            this.metroLabel4.Text = "Total:";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(50, 609);
            this.lblTotal.MaximumSize = new System.Drawing.Size(100, 20);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(46, 20);
            this.lblTotal.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblTotal.TabIndex = 14;
            this.lblTotal.Text = "10000";
            this.lblTotal.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // ProjectMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.btnProposal);
            this.Controls.Add(this.projectList);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.comboCategory);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.endDatePicker);
            this.Controls.Add(this.startDatePicker);
            this.Controls.Add(this.metroLabel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ProjectMetroPanel";
            this.Size = new System.Drawing.Size(868, 633);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DateTimePicker startDatePicker;
        private System.Windows.Forms.DateTimePicker endDatePicker;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox comboCategory;
        private MetroFramework.Controls.MetroButton btnAll;
        private System.Windows.Forms.FlowLayoutPanel projectList;
        private MetroFramework.Controls.MetroTile btnProposal;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel lblTotal;
    }
}
