﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Business;

namespace Citizen {
    public partial class UsersMetroPanel : MetroUserControl {

        UserBUS userBUS;
        public UsersMetroPanel() {
            InitializeComponent();
            userBUS = new UserBUS();

            comboCommunity.Items.Insert(0, "---");
            foreach (Forum forum in new ForumBUS().GetAll())
                comboCommunity.Items.Add(forum.Forum_name);

            Fill();

        }

        public void Fill() {
            List<User> users = userBUS.GetUsers();
            userList.Controls.Clear();
            foreach(User user in users){
                userList.Controls.Add(new UserItem(user));
            }

            lblTotal.Text = userList.Controls.Count.ToString();
        }

        private void OpenFavorites() {

        }

        private void BtnPassUpdate_Click(object sender, EventArgs e) {

        }

        private void Search_Enter(object sender, EventArgs e) {
            if (txtSearch.Text == "Search...")
                txtSearch.Text = "";
        }

        private void Search_Leave(object sender, EventArgs e) {
            if (txtSearch.Text == "")
                txtSearch.Text = "Seach...";
        }

        private void ComboCommunity_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void Text_Changed(object sender, EventArgs e) {

        }
    }
}
