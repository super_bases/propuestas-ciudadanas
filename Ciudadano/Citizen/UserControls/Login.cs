﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Citizen.Business;
using MetroFramework.Controls;
using MetroFramework.Forms;


namespace Citizen {


    public partial class Login : MetroForm {
        UserBUS userBUS;
        private readonly Dictionary<string, string> defaultText = new Dictionary<string, string>() {
            {"txtUsername", "Username"},
            {"txtPassword", "Password"},
        };

        public Login() {
            InitializeComponent();
            userBUS = new UserBUS();
            //user = userBUS.GetPasswordByUsername(txtUsername.Text);
            //txtUsername.Text = user.Username;
            //txtPassword.Text = user.Password;
        }

        private void OnEnter(object sender, EventArgs e) {
            MetroTextBox item = sender as MetroTextBox;
            if (item.Text == defaultText[item.Name]) {
                item.Text = "";
                if (item.Name is "txtPassword")
                    item.PasswordChar = '•';
            }
        }

        private void OnLeave(object sender, EventArgs e) {
            MetroTextBox item = sender as MetroTextBox;
            if (item.Text is "") {
                item.Text = defaultText[item.Name];
                if (item.Name is "txtPassword")
                    item.PasswordChar = '\0';
            }
        }

        static string encrypt(string rawData) {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create()) {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++) {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e) {
            User user = userBUS.GetUserByUsername(txtUsername.Text);
            //txtPassword.Text = user.Password;
            try {

                string enciptada = encrypt(txtPassword.Text);
                if (user.Password.Equals(enciptada)) {
                    LU.User = user;
                    user.Password = txtPassword.Text;
                    Main main = new Main(this);
                    Visible = false;
                    // MessageBox.Show("usn: " + LU.GetU().Username.ToString() + "   pass: " + LU.GetU().Password.ToString());
                    // Dispose();
                    //MessageBox.Show(txtPassword.Text);
                } else {
                    ErrorMsg er = new ErrorMsg("Check your values");
                    er.Show();
                }
            } catch (Exception ex) {
                ErrorMsg er = new ErrorMsg("");
                er.Show();
            }
            //Dispose();
        }

        private void BtnRegister_Click(object sender, EventArgs e) {
            // open profile mode 3
            Register reg = new Register();
            reg.Show();


        }
    }
}
