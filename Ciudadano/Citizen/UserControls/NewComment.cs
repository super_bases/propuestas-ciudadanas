﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Business;
using System.Diagnostics;

namespace Citizen {
    public partial class NewComment : MetroUserControl {
        private readonly ProjectItemView parent;
        private MessageBUS message;
        public NewComment(ProjectItemView item) {
            InitializeComponent();
            parent = item;
            parent.Visible = false;
            message = new MessageBUS();
        }

        private void BtnCancel_Click(object sender, EventArgs e) {
            parent.Visible = true;
            Dispose();
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            // crear comentario
            parent.Visible = true;
            Message msg = new Message() {
                Username = LU.User.Username,
                Project_id = parent.project.Project_id,
                Publish_date = DateTime.Now.ToString("yyyy/MM/dd"),
                Text = txtComment.Text
            };
            Debug.WriteLine(msg.Publish_date);
            message.InsertMessage(msg);
            parent.LoadComments();
            Dispose();
        }

    }
}
