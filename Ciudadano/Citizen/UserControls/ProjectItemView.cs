﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Diagnostics;
using Citizen.Business;

namespace Citizen {
    public partial class ProjectItemView : MetroUserControl {

        private ProjectBUS projectBUS;
        private readonly ProjectItem parent;
        internal Project project;
        public ProjectItemView(ProjectItem item, int project_id) {
            InitializeComponent();
            parent = item;
            parent.Parent.Parent.Visible = false;
            project = new ProjectBUS().GetProjectByID(project_id);
            projectBUS = new ProjectBUS();
            SetTextValues();

            LoadComments();
        }

        public void LoadComments() {
            commentList.Controls.Clear();
            foreach(Message msg in new MessageBUS().GetMessagesByProjectID(project.Project_id)) {
                commentList.Controls.Add(new CommentItem(parent) {
                    Username = msg.Username,
                    Date = msg.Publish_date,
                    Comment = msg.Text
                });
            }
        }

        private void SetTextValues() {
            txtDescription.Text = project.Summary;
            lblCategory.Text = new CategoryBUS().GetCategoryByID(project.Category_id).Category_name;
            lblProjectCreator.Text = project.Username;
            lblProjectName.Text = project.Project_name;
            lblVotes.Text = project.Votes.ToString();
            lblBudget.Text = project.Budget.ToString();
        }

        private void BtnBack_Click(object sender, EventArgs e) {
            parent.Parent.Parent.Visible = true;
            Dispose();
        }

        private void BtnComment_Click(object sender, EventArgs e) {
            NewComment comment = new NewComment(this) {
                Parent = parent.Parent.Parent.Parent,
                Visible = true
            };
            Debug.WriteLine(parent.Parent.Parent.Name);
            parent.Parent.Parent.Parent.Controls.Add(comment);
            parent.Parent.Parent.Visible = false;
        }

        private void BtnVote_Click(object sender, EventArgs e) {
            if(project.Votes < 1000000) {
                projectBUS.InsertVote(project);
                project = projectBUS.GetProjectByID(project.Project_id);
                lblVotes.Text = project.Votes.ToString();
            }
        }
    }
}
