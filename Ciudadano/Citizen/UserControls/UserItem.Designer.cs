﻿namespace Citizen {
    partial class UserItem {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblFullName = new MetroFramework.Controls.MetroLabel();
            this.lblUsername = new MetroFramework.Controls.MetroLabel();
            this.lblCommunity = new MetroFramework.Controls.MetroLabel();
            this.btnView = new MetroFramework.Controls.MetroTile();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblDays = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // lblFullName
            // 
            this.lblFullName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblFullName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblFullName.Location = new System.Drawing.Point(3, 4);
            this.lblFullName.Margin = new System.Windows.Forms.Padding(3);
            this.lblFullName.MaximumSize = new System.Drawing.Size(600, 30);
            this.lblFullName.MinimumSize = new System.Drawing.Size(600, 30);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(600, 30);
            this.lblFullName.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblFullName.TabIndex = 0;
            this.lblFullName.Text = "FULL NAME LAST NAME";
            this.lblFullName.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblUsername
            // 
            this.lblUsername.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblUsername.Location = new System.Drawing.Point(3, 34);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(3);
            this.lblUsername.MaximumSize = new System.Drawing.Size(600, 30);
            this.lblUsername.MinimumSize = new System.Drawing.Size(600, 30);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(600, 30);
            this.lblUsername.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "USERNAME";
            this.lblUsername.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCommunity
            // 
            this.lblCommunity.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblCommunity.Location = new System.Drawing.Point(3, 70);
            this.lblCommunity.Margin = new System.Windows.Forms.Padding(3);
            this.lblCommunity.MaximumSize = new System.Drawing.Size(600, 30);
            this.lblCommunity.MinimumSize = new System.Drawing.Size(600, 30);
            this.lblCommunity.Name = "lblCommunity";
            this.lblCommunity.Size = new System.Drawing.Size(600, 30);
            this.lblCommunity.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblCommunity.TabIndex = 2;
            this.lblCommunity.Text = "USERNAME";
            this.lblCommunity.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(662, 14);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(150, 50);
            this.btnView.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnView.TabIndex = 3;
            this.btnView.Text = "VIEW";
            this.btnView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnView.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Location = new System.Drawing.Point(490, 70);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(202, 17);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Days since last password update:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblDays
            // 
            this.lblDays.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblDays.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblDays.Location = new System.Drawing.Point(712, 70);
            this.lblDays.Margin = new System.Windows.Forms.Padding(3);
            this.lblDays.MaximumSize = new System.Drawing.Size(100, 30);
            this.lblDays.MinimumSize = new System.Drawing.Size(100, 30);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(100, 30);
            this.lblDays.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblDays.TabIndex = 5;
            this.lblDays.Text = "10000000000000";
            this.lblDays.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // UserItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.lblDays);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.lblCommunity);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblFullName);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.Name = "UserItem";
            this.Size = new System.Drawing.Size(860, 110);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblFullName;
        private MetroFramework.Controls.MetroLabel lblUsername;
        private MetroFramework.Controls.MetroLabel lblCommunity;
        private MetroFramework.Controls.MetroTile btnView;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblDays;
    }
}
