﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Business;
using System.Diagnostics;

namespace Citizen {


    public partial class CatalogMod : MetroUserControl {

        CatalogMetroPanel p;
        private int mode = 0;

        #region catalogs
        readonly CategoryBUS category;
        readonly CountryBUS country;
        readonly ProvinceBUS province;
        readonly CantonBUS canton;
        readonly DistrictBUS district;
        readonly NationalityBUS nationality;
        #endregion

        public CatalogMod(string name, CatalogMetroPanel panel) {
            InitializeComponent();
            Name = name;
            comboCanton.Enabled = false;
            comboCountry.Enabled = false;
            comboProvince.Enabled = false;
            txtValue.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;

            country = new CountryBUS();
            category = new CategoryBUS();
            district = new DistrictBUS();
            canton = new CantonBUS();
            province = new ProvinceBUS();
            category = new CategoryBUS();
            nationality = new NationalityBUS();

            string[] columnHeaders = new string[] { "ID", "", "Date Added", "by", "Date Added", "by", "Status" };
            columnHeaders[1] = name;

            columnHeaders.ToList().ForEach(x => itemList.Columns.Add(x));

            // resize columns
            itemList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            p = panel;

        }

        private void BtnAdd_Click(object sender, EventArgs e) {
            // load values on combo
            // only enable the needed ones

            comboCanton.Enabled = true;
            comboCountry.Enabled = true;
            comboProvince.Enabled = true;
            txtValue.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            mode = 1;
            txtValue.Text = "";
        }

        private void BtnEdit_Click(object sender, EventArgs e) {
            comboCanton.Enabled = true;
            comboCountry.Enabled = true;
            comboProvince.Enabled = true;
            txtValue.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            mode = 2;

        }

        private void BtnEnable_Click(object sender, EventArgs e) {
            if (txtValue.Text != "") {
                switch (Name) {
                    case "Country":
                        Country c = country.GetCountryByID(int.Parse(itemList.SelectedItems[0].Text));
                        country.EditCountry(c.Id, c.Name, c.Status == 1 ? 0 : 1);
                        break;
                    case "Category":
                        Category cat = category.GetCategoryByID(int.Parse(itemList.SelectedItems[0].Text));
                        category.EditCategory(cat.Category_id, cat.Category_name, cat.Status == 1 ? 0 : 1);
                        break;
                    case "Nationality":
                        Nationality n = nationality.GetNationalityByID(int.Parse(itemList.SelectedItems[0].Text));
                        nationality.EditNationality(n.Nationality_id, n.Nationality_name, n.Status == 1 ? 0 : 1);
                        break;
                    case "Province":
                        // hacer
                        break;
                    case "District":
                        // hacer
                        break;
                    case "Canton":
                        // hacer
                        break;
                }
            }
            p.UpdateList(Name);
            txtValue.Text = "";
        }

        private void BtnDelete_Click(object sender, EventArgs e) {
            switch (Name) {
                case "Country":
                    country.DeleteCountry(int.Parse(itemList.SelectedItems[0].Text));
                    break;
                case "Nationality":
                    nationality.DeleteNationality(int.Parse(itemList.SelectedItems[0].Text));
                    break;
                case "Category":
                    category.DeleteCategory(int.Parse(itemList.SelectedItems[0].Text));
                    break;
                case "Province":
                    province.DeleteProvince(int.Parse(itemList.SelectedItems[0].Text));
                    break;
                case "District":
                    district.DeleteDistrict(int.Parse(itemList.SelectedItems[0].Text));
                    break;
                case "Canton":
                    canton.DeleteCanton(int.Parse(itemList.SelectedItems[0].Text));
                    break;
            }
            p.UpdateList(Name);
        }

        private void BtnCancel_Click(object sender, EventArgs e) {
            comboCanton.Enabled = false;
            comboCountry.Enabled = false;
            comboProvince.Enabled = false;
            txtValue.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            comboCanton.Enabled = false;
            comboCountry.Enabled = false;
            comboProvince.Enabled = false;
            txtValue.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;

            if (txtValue.Text != "" && mode == 1) {
                switch (Name) {
                    case "Country":
                        country.InsertCountry(txtValue.Text);
                        break;
                    case "Category":
                        category.InsertCategory(txtValue.Text);
                        break;
                    case "Nationality":
                        nationality.InsertNationality(txtValue.Text);
                        break;
                    case "Province":
                        // hacer
                        break;
                    case "District":
                        // hacer
                        break;
                    case "Canton":
                        // hacer
                        break;
                }
            }
            if (mode == 2) {
                if (txtValue.Text != "") {
                    switch (Name) {
                        case "Country":
                            country.EditCountry(int.Parse(itemList.SelectedItems[0].Text), txtValue.Text, 1);
                            break;
                        case "Category":
                            category.EditCategory(int.Parse(itemList.SelectedItems[0].Text), txtValue.Text, 1);
                            break;
                        case "Nationality":
                            nationality.EditNationality(int.Parse(itemList.SelectedItems[0].Text), txtValue.Text, 1);
                            break;
                        case "Province":
                            // hacer
                            break;
                        case "District":
                            // hacer
                            break;
                        case "Canton":
                            // hacer
                            break;
                    }
                }
            }
            mode = 0;
            txtValue.Text = "";
            p.UpdateList(Name);
        }

        private void Item_selected(object sender, EventArgs e) {
            txtValue.Text = itemList.SelectedItems[0].SubItems[1].Text;
        }
    }
}
