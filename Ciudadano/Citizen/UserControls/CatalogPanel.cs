﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Business;
using Citizen.Data_Access;

namespace Citizen {
    public partial class CatalogMetroPanel : MetroUserControl {

        private string value; // assign current top value

        #region catalogs
        readonly ParameterBUS parameter;
        readonly CategoryBUS category;
        readonly CountryBUS country;
        readonly ProvinceBUS province;
        readonly CantonBUS canton;
        readonly DistrictBUS district;
        readonly NationalityBUS nationality;
        #endregion

        #region catalogs tabs
        readonly CatalogMod catTab;
        readonly CatalogMod conTab;
        readonly CatalogMod proTab;
        readonly CatalogMod canTab;
        readonly CatalogMod disTab;
        readonly CatalogMod natTab;
        #endregion

        public CatalogMetroPanel() {
            InitializeComponent();
            txtValue.Enabled = false;

            catTab= new CatalogMod("Category", this);
            conTab= new CatalogMod("Country", this);
            proTab= new CatalogMod("Province", this);
            canTab= new CatalogMod("Canton", this);
            disTab= new CatalogMod("District", this);
            natTab= new CatalogMod("Nationality", this);

            tabControl.TabPages["CountryTab"].Controls.Add(conTab);
            tabControl.TabPages["ProvinceTab"].Controls.Add(proTab);
            tabControl.TabPages["CantonTab"].Controls.Add(canTab);
            tabControl.TabPages["DistrictTab"].Controls.Add(disTab);
            tabControl.TabPages["CategoryTab"].Controls.Add(catTab);
            tabControl.TabPages["nationalityTab"].Controls.Add(natTab);

            
            country = new CountryBUS();
            parameter = new ParameterBUS();
            category = new CategoryBUS();
            district = new DistrictBUS();
            canton = new CantonBUS();
            province = new ProvinceBUS();
            category = new CategoryBUS();
            nationality = new NationalityBUS();

            FillCatalogs();

        }
        private void FillCatalogs() {
            FillCountry();
            FillCategory();
            FillNationality();
            FillDistrict();
            FillCanton();
            FillProvince();
            SetTop();
        }

        private void FillProvince() {
            ListView list = (ListView)tabControl.TabPages["ProvinceTab"].Controls.Find("Province", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (Province cat in province.GetProvinces()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Id.ToString(), cat.Name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void FillCanton() {
            ListView list = (ListView)tabControl.TabPages["CantonTab"].Controls.Find("Canton", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (Canton cat in canton.GetCantons()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Id.ToString(), cat.Name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        internal void UpdateList(string name) {
            switch (name) {
                case "Country":
                    FillCountry();
                    break;
                case "Nationality":
                    FillNationality();
                    break;
                case "Category":
                    FillCategory();
                    break;
                case "District":
                    FillDistrict();
                    break;
                case "Canton":
                    FillCanton();
                    break;
                case "Province":
                    FillProvince();
                    break;
            }
        }

        private void FillDistrict() {
            ListView list = (ListView)tabControl.TabPages["DistrictTab"].Controls.Find("District", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (District cat in district.GetDistricts()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Id.ToString(), cat.Name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void FillNationality() {
            ListView list = (ListView)tabControl.TabPages["NationalityTab"].Controls.Find("Nationality", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (Nationality cat in nationality.GetNationalities()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Nationality_id.ToString(), cat.Nationality_name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user, cat.Status.ToString() }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void SetTop() {
            value = txtValue.Text = parameter.GetParameter().Parameter_value;
        }

        private void FillCountry() {
            ListView list = (ListView)tabControl.TabPages["CountryTab"].Controls.Find("Country", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (Country cat in country.getCountries()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Id.ToString(), cat.Name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user, cat.Status.ToString() }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void FillCategory() {
            ListView list = (ListView)tabControl.TabPages["CategoryTab"].Controls.Find("Category", false).First().Controls.Find("itemList", false).First();
            list.Items.Clear();
            foreach (Category cat in category.GetCategories()) {
                list.Items.Add(new ListViewItem(new string[] { cat.Category_id.ToString(), cat.Category_name, cat.Creation_date, cat.Creation_user, cat.Last_modified, cat.Last_modified_user, cat.Status.ToString() }));
            };
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            list.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void BtnEdit_Click(object sender, EventArgs e) {
            txtValue.Enabled = true;
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            txtValue.Enabled = false;
        }

        private void BtnCancel_Click(object sender, EventArgs e) {
            txtValue.Enabled = false;
        }

        private void Value_Leave(object sender, EventArgs e) {
            if (txtValue.Text == "")
                txtValue.Text = value;
            else
                value = txtValue.Text;
        }
    }
}
