﻿namespace Citizen {
    partial class CatalogMod {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnEnable = new MetroFramework.Controls.MetroButton();
            this.btnAdd = new MetroFramework.Controls.MetroButton();
            this.btnDelete = new MetroFramework.Controls.MetroButton();
            this.btnEdit = new MetroFramework.Controls.MetroButton();
            this.itemList = new System.Windows.Forms.ListView();
            this.comboCountry = new MetroFramework.Controls.MetroComboBox();
            this.comboProvince = new MetroFramework.Controls.MetroComboBox();
            this.comboCanton = new MetroFramework.Controls.MetroComboBox();
            this.txtValue = new MetroFramework.Controls.MetroTextBox();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // btnEnable
            // 
            this.btnEnable.Location = new System.Drawing.Point(581, 107);
            this.btnEnable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEnable.Name = "btnEnable";
            this.btnEnable.Size = new System.Drawing.Size(221, 42);
            this.btnEnable.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnEnable.TabIndex = 17;
            this.btnEnable.Text = "Enable/Disable";
            this.btnEnable.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEnable.Click += new System.EventHandler(this.BtnEnable_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(581, 11);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(221, 42);
            this.btnAdd.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Text = "Add";
            this.btnAdd.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(581, 155);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(221, 42);
            this.btnDelete.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(581, 59);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(221, 42);
            this.btnEdit.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnEdit.TabIndex = 14;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // itemList
            // 
            this.itemList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.itemList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.itemList.FullRowSelect = true;
            this.itemList.HideSelection = false;
            this.itemList.Location = new System.Drawing.Point(3, 11);
            this.itemList.Margin = new System.Windows.Forms.Padding(0);
            this.itemList.Name = "itemList";
            this.itemList.Size = new System.Drawing.Size(561, 556);
            this.itemList.TabIndex = 13;
            this.itemList.UseCompatibleStateImageBehavior = false;
            this.itemList.View = System.Windows.Forms.View.Details;
            this.itemList.Click += new System.EventHandler(this.Item_selected);
            // 
            // comboCountry
            // 
            this.comboCountry.FormattingEnabled = true;
            this.comboCountry.ItemHeight = 24;
            this.comboCountry.Location = new System.Drawing.Point(581, 226);
            this.comboCountry.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboCountry.Name = "comboCountry";
            this.comboCountry.Size = new System.Drawing.Size(221, 30);
            this.comboCountry.TabIndex = 18;
            // 
            // comboProvince
            // 
            this.comboProvince.FormattingEnabled = true;
            this.comboProvince.ItemHeight = 24;
            this.comboProvince.Location = new System.Drawing.Point(581, 266);
            this.comboProvince.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboProvince.Name = "comboProvince";
            this.comboProvince.Size = new System.Drawing.Size(221, 30);
            this.comboProvince.TabIndex = 19;
            // 
            // comboCanton
            // 
            this.comboCanton.FormattingEnabled = true;
            this.comboCanton.ItemHeight = 24;
            this.comboCanton.Location = new System.Drawing.Point(581, 306);
            this.comboCanton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboCanton.Name = "comboCanton";
            this.comboCanton.Size = new System.Drawing.Size(221, 30);
            this.comboCanton.TabIndex = 20;
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(581, 366);
            this.txtValue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtValue.MaxLength = 25;
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(221, 30);
            this.txtValue.TabIndex = 21;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(581, 466);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(221, 30);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(581, 431);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(221, 30);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // CatalogMod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.comboCanton);
            this.Controls.Add(this.comboProvince);
            this.Controls.Add(this.comboCountry);
            this.Controls.Add(this.btnEnable);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.itemList);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "CatalogMod";
            this.Size = new System.Drawing.Size(851, 570);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroButton btnEnable;
        private MetroFramework.Controls.MetroButton btnAdd;
        private MetroFramework.Controls.MetroButton btnDelete;
        private MetroFramework.Controls.MetroButton btnEdit;
        private System.Windows.Forms.ListView itemList;
        private MetroFramework.Controls.MetroComboBox comboCountry;
        private MetroFramework.Controls.MetroComboBox comboProvince;
        private MetroFramework.Controls.MetroComboBox comboCanton;
        private MetroFramework.Controls.MetroTextBox txtValue;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroTile btnSave;
    }
}
