﻿namespace Citizen {
    partial class CommentItem {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblUsername = new MetroFramework.Controls.MetroLabel();
            this.lblDate = new MetroFramework.Controls.MetroLabel();
            this.txtText = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // lblUsername
            // 
            this.lblUsername.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblUsername.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblUsername.Location = new System.Drawing.Point(2, 0);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUsername.MaximumSize = new System.Drawing.Size(412, 24);
            this.lblUsername.MinimumSize = new System.Drawing.Size(412, 24);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(412, 24);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "USERNAME";
            this.lblUsername.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDate.Location = new System.Drawing.Point(424, 97);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.MaximumSize = new System.Drawing.Size(200, 16);
            this.lblDate.MinimumSize = new System.Drawing.Size(200, 16);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(200, 16);
            this.lblDate.TabIndex = 2;
            this.lblDate.Text = "00/00/000 00:00 pppp";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDate.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // txtText
            // 
            this.txtText.Enabled = false;
            this.txtText.Location = new System.Drawing.Point(3, 27);
            this.txtText.MaxLength = 200;
            this.txtText.Multiline = true;
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(621, 60);
            this.txtText.TabIndex = 3;
            this.txtText.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // CommentItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtText);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblUsername);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.MaximumSize = new System.Drawing.Size(645, 120);
            this.MinimumSize = new System.Drawing.Size(645, 120);
            this.Name = "CommentItem";
            this.Size = new System.Drawing.Size(645, 120);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblUsername;
        private MetroFramework.Controls.MetroLabel lblDate;
        private MetroFramework.Controls.MetroTextBox txtText;
    }
}
