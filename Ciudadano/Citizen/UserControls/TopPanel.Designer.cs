﻿namespace Citizen {
    partial class TopMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.communityList = new System.Windows.Forms.ListView();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.community = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.posts = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.country = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.province = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // communityList
            // 
            this.communityList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.communityList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.communityList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.community,
            this.country,
            this.province,
            this.posts});
            this.communityList.Location = new System.Drawing.Point(0, 48);
            this.communityList.Name = "communityList";
            this.communityList.Size = new System.Drawing.Size(865, 582);
            this.communityList.TabIndex = 0;
            this.communityList.UseCompatibleStateImageBehavior = false;
            this.communityList.View = System.Windows.Forms.View.Details;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(164, 25);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Top communities";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // community
            // 
            this.community.Text = "Community";
            // 
            // posts
            // 
            this.posts.Text = "# of posts";
            // 
            // country
            // 
            this.country.Text = "Country";
            // 
            // province
            // 
            this.province.Text = "Province";
            // 
            // TopMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.communityList);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TopMetroPanel";
            this.Size = new System.Drawing.Size(868, 633);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView communityList;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.ColumnHeader community;
        private System.Windows.Forms.ColumnHeader country;
        private System.Windows.Forms.ColumnHeader posts;
        private System.Windows.Forms.ColumnHeader province;
    }
}
