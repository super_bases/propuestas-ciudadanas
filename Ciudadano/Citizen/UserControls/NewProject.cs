﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Business;
using Citizen.Data_Access;

namespace Citizen {
    public partial class NewProject : MetroUserControl {

        ProjectBUS projectBUS;

        private readonly Dictionary<string, string> defaultText = new Dictionary<string, string>() {
            {"txtBudget", "Budget"},
            {"txtDescription", "Write your description here..."},
            {"txtProjectName", "Title"}
        };

        private readonly ProjectMetroPanel parent;
        public NewProject(ProjectMetroPanel parent) {
            InitializeComponent();
            this.parent = parent;
            parent.Visible = false;

            DataTable data = new CategoryDAO().GetAll();
            comboCategory.DataSource = data;
            comboCategory.DisplayMember = "category_name";
            comboCategory.ValueMember = "category_id";

            projectBUS = new ProjectBUS();
            // txtProjectName.Text.DefaultIfEmpty();
        }

        private void BtnSave_Click(object sender, EventArgs e) {
            // validate data, mejor si se hace cuando la gente escribe
            // create new project
            Project project = new Project() {
                Project_name = txtProjectName.Text,
                Summary = txtDescription.Text,
                Budget = int.Parse(txtBudget.Text),
                Votes = 0,
                Username = LU.User.Username,
                Category_id = int.Parse(comboCategory.SelectedValue.ToString()),
                Forum_id = Community.Forum.Forum_id,
            };
            projectBUS.InsertProject(project);
            parent.Fill(new ProjectBUS().GetProjectsByForumID(Community.Forum.Forum_id));
            parent.Visible = true;
            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e) {
            parent.Visible = true;
            Dispose();
        }

        private void TextBox_Enter(object sender, EventArgs e) {
            MetroTextBox item = sender as MetroTextBox;
            if (item.Text == defaultText[item.Name])
                item.Text = "";
        }

        private void TextBox_Leave(object sender, EventArgs e) {
            MetroTextBox item = sender as MetroTextBox;
            if (item.Text is "")
                item.Text = defaultText[item.Name];
        }
    }
}
