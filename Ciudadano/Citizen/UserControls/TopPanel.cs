﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Citizen {
    public partial class TopMetroPanel : MetroUserControl {
        public TopMetroPanel() {
            InitializeComponent();

            for (int i = 0; i < 30; i++) {
                communityList.Items.Add("community " + i).SubItems.AddRange(new string[] { "country", "province","100"});
            }

            communityList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            communityList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
    }
}
