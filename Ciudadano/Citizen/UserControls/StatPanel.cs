﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Citizen.Data_Access;
using Citizen.Business;
using System.Diagnostics;

namespace Citizen {
    public partial class StatMetroPanel : MetroUserControl {
        public StatMetroPanel() {
            InitializeComponent();
        }


        private void LoadCategoryChart() {
            DataTable table = new ProjectDAO().GetProjectCountByCategory();
            pieChart.Series["pieChart1"].Points.Clear();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(project_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(new CategoryBUS().GetCategoryByID(int.Parse(row["category_id"].ToString())).Category_name, per);
            }
        }

        private void BtnCategory_Click(object sender, EventArgs e) {
            LoadCategoryChart();
        }

        private void BtnProvinceP_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new ProjectDAO().CountByProvince();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_province.province_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnCountryP_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new ProjectDAO().CountByCountry();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_country.country_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnCantonP_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new ProjectDAO().CountByCanton();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_canton.canton_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnProvinceU_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new PersonDAO().COuntByProvince();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_province.province_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnCountryU_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new PersonDAO().COuntByCountry();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_country.country_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnCantonU_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new PersonDAO().CountByCanton();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["count(tbl_canton.canton_id)"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }

        private void BtnDistrictU_Click(object sender, EventArgs e) {
            pieChart.Series["pieChart1"].Points.Clear();
            DataTable table = new PersonDAO().CountByDistrict();
            int total = table.Rows.Count;
            float per = 0;
            foreach (DataRow row in table.Rows) {
                per = (int.Parse(row["amount"].ToString()) * 1.0f) / total * 100;
                Debug.WriteLine(per);
                pieChart.Series["pieChart1"].Points.AddXY(row["name"].ToString(), per);
            }
        }


        private void BtnAge_Click(object sender, EventArgs e) {
            int total = new PersonBUS().GetAll().Count;
            pieChart.Series["pieChart1"].Points.Clear();
            pieChart.Series["pieChart1"].Points.AddXY("0 - 18", (new PersonBUS().CountAgeRange(0, 18) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("19 - 30", (new PersonBUS().CountAgeRange(19, 30) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("34 - 45", (new PersonBUS().CountAgeRange(31, 45) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("45 - 55", (new PersonBUS().CountAgeRange(46, 55) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("56 - 65", (new PersonBUS().CountAgeRange(56, 65) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("66 - 75", (new PersonBUS().CountAgeRange(66, 75) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("76 - 85", (new PersonBUS().CountAgeRange(76, 85) * 1.0f) / total * 100);
            pieChart.Series["pieChart1"].Points.AddXY("86+", (new PersonBUS().CountAgeRange(86, 500) * 1.0f) / total * 100);
        }
    }
}
