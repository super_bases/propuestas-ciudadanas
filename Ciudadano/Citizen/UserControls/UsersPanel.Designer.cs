﻿namespace Citizen {
    partial class UsersMetroPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.userList = new System.Windows.Forms.FlowLayoutPanel();
            this.txtSearch = new MetroFramework.Controls.MetroTextBox();
            this.comboCommunity = new MetroFramework.Controls.MetroComboBox();
            this.btnPassUpdate = new MetroFramework.Controls.MetroButton();
            this.lblTotal = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // userList
            // 
            this.userList.AutoScroll = true;
            this.userList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.userList.Location = new System.Drawing.Point(3, 66);
            this.userList.Name = "userList";
            this.userList.Size = new System.Drawing.Size(862, 534);
            this.userList.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtSearch.Location = new System.Drawing.Point(3, 30);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(289, 30);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.Text = "Search...";
            this.txtSearch.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtSearch.TextChanged += new System.EventHandler(this.Text_Changed);
            this.txtSearch.Enter += new System.EventHandler(this.Search_Enter);
            this.txtSearch.Leave += new System.EventHandler(this.Search_Leave);
            // 
            // comboCommunity
            // 
            this.comboCommunity.FormattingEnabled = true;
            this.comboCommunity.ItemHeight = 24;
            this.comboCommunity.Location = new System.Drawing.Point(298, 30);
            this.comboCommunity.Name = "comboCommunity";
            this.comboCommunity.Size = new System.Drawing.Size(318, 30);
            this.comboCommunity.Style = MetroFramework.MetroColorStyle.Teal;
            this.comboCommunity.TabIndex = 2;
            this.comboCommunity.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.comboCommunity.SelectedIndexChanged += new System.EventHandler(this.ComboCommunity_SelectedIndexChanged);
            // 
            // btnPassUpdate
            // 
            this.btnPassUpdate.Location = new System.Drawing.Point(623, 30);
            this.btnPassUpdate.Name = "btnPassUpdate";
            this.btnPassUpdate.Size = new System.Drawing.Size(242, 30);
            this.btnPassUpdate.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnPassUpdate.TabIndex = 3;
            this.btnPassUpdate.Text = "Password Update";
            this.btnPassUpdate.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnPassUpdate.Click += new System.EventHandler(this.BtnPassUpdate_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(49, 603);
            this.lblTotal.MaximumSize = new System.Drawing.Size(200, 20);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(126, 20);
            this.lblTotal.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblTotal.TabIndex = 16;
            this.lblTotal.Text = "100000000000000";
            this.lblTotal.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 603);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(40, 20);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel4.TabIndex = 15;
            this.metroLabel4.Text = "Total:";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // UsersMetroPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.btnPassUpdate);
            this.Controls.Add(this.comboCommunity);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.userList);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UsersMetroPanel";
            this.Size = new System.Drawing.Size(868, 633);
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel userList;
        private MetroFramework.Controls.MetroTextBox txtSearch;
        private MetroFramework.Controls.MetroComboBox comboCommunity;
        private MetroFramework.Controls.MetroButton btnPassUpdate;
        private MetroFramework.Controls.MetroLabel lblTotal;
        private MetroFramework.Controls.MetroLabel metroLabel4;
    }
}
