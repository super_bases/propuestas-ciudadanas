﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;

namespace Citizen {
    public partial class PassLogControl : MetroForm  {
        public PassLogControl(string username) {
            InitializeComponent();
            DataTable pass_data = new Data_Access.PersonDAO().GetPassLog(username);
            for (int i = 0; i < pass_data.Rows.Count; i++) {
                DataRow row = pass_data.Rows[i];
                String user = row["username"].ToString();
                String fecha = row["change_date"].ToString();
                ListViewItem listitem = new ListViewItem(fecha);
                listitem.SubItems.Add(user);
                log.Items.Add(listitem);
            }

        }
    }
}
