﻿namespace Citizen {
    partial class ProjectItem {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblProjectName = new MetroFramework.Controls.MetroLabel();
            this.lblProjectCreator = new MetroFramework.Controls.MetroLabel();
            this.lblCategory = new MetroFramework.Controls.MetroLabel();
            this.lblVotes = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.projectItemPanel = new MetroFramework.Controls.MetroPanel();
            this.projectItemPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblProjectName
            // 
            this.lblProjectName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblProjectName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblProjectName.Location = new System.Drawing.Point(16, 12);
            this.lblProjectName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProjectName.MaximumSize = new System.Drawing.Size(533, 31);
            this.lblProjectName.MinimumSize = new System.Drawing.Size(533, 31);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(533, 31);
            this.lblProjectName.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProjectName.TabIndex = 2;
            this.lblProjectName.Text = "PROJECT NAME";
            this.lblProjectName.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblProjectCreator
            // 
            this.lblProjectCreator.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblProjectCreator.Location = new System.Drawing.Point(16, 43);
            this.lblProjectCreator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProjectCreator.MaximumSize = new System.Drawing.Size(533, 31);
            this.lblProjectCreator.MinimumSize = new System.Drawing.Size(533, 31);
            this.lblProjectCreator.Name = "lblProjectCreator";
            this.lblProjectCreator.Size = new System.Drawing.Size(533, 31);
            this.lblProjectCreator.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblProjectCreator.TabIndex = 3;
            this.lblProjectCreator.Text = "PROJECT CREATOR";
            this.lblProjectCreator.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCategory
            // 
            this.lblCategory.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblCategory.Location = new System.Drawing.Point(16, 81);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategory.MaximumSize = new System.Drawing.Size(533, 31);
            this.lblCategory.MinimumSize = new System.Drawing.Size(533, 31);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(533, 31);
            this.lblCategory.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblCategory.TabIndex = 4;
            this.lblCategory.Text = "CATEGORY";
            this.lblCategory.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblVotes
            // 
            this.lblVotes.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblVotes.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblVotes.Location = new System.Drawing.Point(712, 12);
            this.lblVotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVotes.MaximumSize = new System.Drawing.Size(93, 31);
            this.lblVotes.MinimumSize = new System.Drawing.Size(93, 31);
            this.lblVotes.Name = "lblVotes";
            this.lblVotes.Size = new System.Drawing.Size(93, 31);
            this.lblVotes.Style = MetroFramework.MetroColorStyle.Teal;
            this.lblVotes.TabIndex = 6;
            this.lblVotes.Text = "TTTTT";
            this.lblVotes.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblVotes.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(655, 16);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.MaximumSize = new System.Drawing.Size(533, 31);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(49, 20);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "Votes:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTile1
            // 
            this.metroTile1.Location = new System.Drawing.Point(655, 49);
            this.metroTile1.Margin = new System.Windows.Forms.Padding(4);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(151, 44);
            this.metroTile1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTile1.TabIndex = 8;
            this.metroTile1.Text = "VIEW";
            this.metroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile1.Click += new System.EventHandler(this.MetroTile1_Click);
            // 
            // projectItemPanel
            // 
            this.projectItemPanel.Controls.Add(this.metroTile1);
            this.projectItemPanel.Controls.Add(this.metroLabel1);
            this.projectItemPanel.Controls.Add(this.lblVotes);
            this.projectItemPanel.Controls.Add(this.lblCategory);
            this.projectItemPanel.Controls.Add(this.lblProjectCreator);
            this.projectItemPanel.Controls.Add(this.lblProjectName);
            this.projectItemPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectItemPanel.HorizontalScrollbarBarColor = true;
            this.projectItemPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.projectItemPanel.HorizontalScrollbarSize = 12;
            this.projectItemPanel.Location = new System.Drawing.Point(0, 0);
            this.projectItemPanel.Margin = new System.Windows.Forms.Padding(0);
            this.projectItemPanel.Name = "projectItemPanel";
            this.projectItemPanel.Size = new System.Drawing.Size(860, 121);
            this.projectItemPanel.TabIndex = 0;
            this.projectItemPanel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.projectItemPanel.VerticalScrollbarBarColor = true;
            this.projectItemPanel.VerticalScrollbarHighlightOnWheel = false;
            this.projectItemPanel.VerticalScrollbarSize = 13;
            // 
            // ProjectItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.projectItemPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(860, 123);
            this.MinimumSize = new System.Drawing.Size(860, 123);
            this.Name = "ProjectItem";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.Size = new System.Drawing.Size(860, 123);
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.projectItemPanel.ResumeLayout(false);
            this.projectItemPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblProjectName;
        private MetroFramework.Controls.MetroLabel lblProjectCreator;
        private MetroFramework.Controls.MetroLabel lblCategory;
        private MetroFramework.Controls.MetroLabel lblVotes;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroPanel projectItemPanel;
    }
}
