﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class PhoneNumberDAO {
        public DataTable SearchPhoneByUser(string user) {
            DataTable person_data = new Data_Access.PersonDAO().SearchByUsername(user);
            DataRow rowPerson = person_data.Rows[0];
            int param = int.Parse(rowPerson["person_id"].ToString());
            string query = string.Format("select * from tbl_phonenumber where person_id = '{0}'", param);
            return DB.Conn.ExecuteSelectQuery(query);
        }

    }
}
