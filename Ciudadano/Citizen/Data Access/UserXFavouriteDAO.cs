﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class UserXFavouriteDAO {
        public DataTable SearchByUsername(string username) {
            string query = string.Format("select * from tbl_UserXFavourite where username = '{0}'", username);
            return DB.Conn.ExecuteSelectQuery(query);
        }
    }
}
