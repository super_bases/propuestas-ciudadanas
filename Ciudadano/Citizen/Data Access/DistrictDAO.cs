﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class DistrictDAO {
        public DataTable SearchDistrictByCantonID(int id) {
            string query = string.Format("select * from tbl_district where canton_id = '{0}' order by district_id asc", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable SearchAllDistricts() {
            string query = string.Format("select * from tbl_district order by district_id asc");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Delete(int v) {
            DB.Conn.CatalogSimpleModInt("district", "delete", v);
        }
    }
}
