﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Citizen.Data_Access {
    class MessageDAO {
        public DataTable Search(string col, int id) {
            string query = string.Format("select * from tbl_message where {0} = '{1}'", col, id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public void Insert(Message msg) {
            DB.Conn.InsertComment(msg);
        }
    }
}
