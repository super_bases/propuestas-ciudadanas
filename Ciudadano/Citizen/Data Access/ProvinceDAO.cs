﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class ProvinceDAO {

        public DataTable SearchProvinceByCountryID(int id) {
            string query = string.Format("select * from tbl_province where country_id = '{0}' order by province_id asc", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchAll() {
            string query = string.Format("select * from tbl_province  order by province_id asc");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchProvinceByCantonID(int id) {
            string query = string.Format("select * from tbl_province where province_id = (select province_id from tbl_canton where canton_id = '{0}')", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Delete(int v) {
            DB.Conn.CatalogSimpleModInt("province", "delete", v);
        }
    }
}
