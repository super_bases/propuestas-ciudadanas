﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class NationalityDAO {

        public DataTable SearchNationalities() {
            string query = string.Format("select * from tbl_nationality");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Insert(string text) {
            DB.Conn.CatalogSimpleModString("nationality", "insert", text);
        }

        internal void Delete(int v) {
            DB.Conn.CatalogSimpleModInt("nationality", "delete", v);
        }

        public DataTable SearchNationalityByID(int id) {
            string query = string.Format("select * from tbl_nationality where nationality_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Edit(int v, string text, int status) {
            DB.Conn.CatalogUpdate("nationality", text, v, status);
        }
    }
}