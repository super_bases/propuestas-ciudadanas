﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Citizen.Data_Access {
    class UserDAO {
        
        public DataTable SearchByUsername(string username) {
            string query = string.Format("select * from tbl_user where username = '{0}'", username);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchAllUsers() {
            string query = string.Format("select * from tbl_user");
            return DB.Conn.ExecuteSelectQuery(query);
        }

    }
}
