﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class PersonDAO {
        public DataTable SearchByUsername(string username) {
            string query = string.Format("select * from tbl_person where username = '{0}'", username);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchForumByCantonID(int id) {
            string query = string.Format("select * from tbl_forum where canton_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable CountByDistrict() {
            string query = "SELECT d.name, COUNT(d.district_id) amount FROM tbl_district d JOIN tbl_person p ON d.district_id = p.district_id GROUP BY  d.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }


        internal DataTable GetPassLog(string id) {
            string query = string.Format("select * from tbl_passwordlog where username = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable COuntByCountry() {
            string query = "SELECT tbl_country.name, count(tbl_country.country_id)" +
                            " FROM tbl_person" +
                            " JOIN tbl_district ON(tbl_person.district_id = tbl_district.district_id)" +
                           " Join tbl_canton on(tbl_canton.canton_id = tbl_district.district_id)" +
                            " Join tbl_province on(tbl_province.province_id = tbl_canton.province_id)" +
                            " Join tbl_country on (tbl_country.country_id = tbl_province.country_id)" +
                           "  group by tbl_country.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable COuntByProvince() {
            string query = "SELECT tbl_province.name, count(tbl_province.province_id)" +
                            " FROM tbl_person" +
                            " JOIN tbl_district ON(tbl_person.district_id = tbl_district.district_id)" +
                           " Join tbl_canton on(tbl_canton.canton_id = tbl_district.district_id)" +
                            " Join tbl_province on(tbl_province.province_id = tbl_canton.province_id)" +
                           " group by tbl_province.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable CountByCanton() {
            string query = "SELECT tbl_canton.name, count(tbl_canton.canton_id) FROM tbl_person JOIN tbl_district ON(tbl_person.district_id = tbl_district.district_id) Join tbl_canton on(tbl_canton.canton_id = tbl_district.district_id) group by tbl_canton.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable AgeRange(int start, int end) {
            string query = string.Format("SELECT count(person_id) FROM tbl_person where TRUNC(MONTHS_BETWEEN(SYSDATE, birthdate))/12 between {0} and {1}", start, end);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchAllPersons() {
            string query = string.Format("select * from tbl_person");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchPersonNationality(string username) {
            DataTable person_data = SearchByUsername(username);
            DataRow rowPerson = person_data.Rows[0];
            int param = int.Parse(rowPerson["person_id"].ToString());
            string query = string.Format("select * from tbl_personxnationality where person_id = '{0}'", param);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void InsertUser(string username, string pass, int type) {
            DB.Conn.UserSimpleModString(username, pass, type);
        }

        internal void InsertPerson(string identification, string name,
                             string flastname, string slastname, string birthdate, string picture,
                             string username, int district_id, int nationality_id, string email, int phonenumber) {
            DB.Conn.PersonSimpleModString(identification, name, flastname, slastname, birthdate, picture, username, district_id, nationality_id, email, phonenumber);
        }


        internal void UpdatePerson(int id, string identification, string name, string flastname, string slastname,
                                   string birthdate, string picture, int district_id, int nationality_id,
                                   int email_id, string email, int phone_id, int phonenumber) {
            DB.Conn.updatePerson(id, identification, name, flastname, slastname, birthdate, picture, district_id, nationality_id, email_id, email, phone_id, phonenumber);
        }

        internal void UpdatePassWord(string username, string newPass) {
            DB.Conn.updatePassWord(username, newPass);
        }

    }
}


/* (p_identification in varchar2, p_name in varchar2, p_firstlastname in varchar2, 
p_secondlastname in varchar2, p_birthdate in varchar2, p_picture in varchar2, p_username varchar2, 
p_district_id in number, p_nationality_id in number, p_email in varchar2, p_phonenumber in number) */
