﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class CategoryDAO {
        public DataTable SearchByID(int id) {
            string query = string.Format("select * from tbl_category where category_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public void Insert(string name) {
            DB.Conn.CatalogSimpleModString("category", "insert", name);
        }

        public void Delete(int id) {
            DB.Conn.CatalogSimpleModInt("category", "delete", id);
        }

        internal DataTable GetAll() {
            string query = string.Format("select * from tbl_category");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Edit(int v, string text, int status) {
            DB.Conn.CatalogUpdate("category", text, v, status);
        }

        internal DataTable GetCategoryByName(string selectedText) {
            string query = string.Format("select * from tbl_category where category_name = '{0}'", selectedText);
            return DB.Conn.ExecuteSelectQuery(query);
        }
    }
}
