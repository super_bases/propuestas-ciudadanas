﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class CountryDAO {
        public DataTable SearchAllCountries() {
            string query = string.Format("select * from tbl_country  order by country_id asc");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchCountryByID(int id) {
            string query = string.Format("select * from tbl_country where country_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }


        internal void Insert(string name) {
            DB.Conn.CatalogSimpleModString("country", "insert", name);
        }

        public void Delete(int id) {
            DB.Conn.CatalogSimpleModInt("country", "delete", id);
        }

        public DataTable SearchCountryByProvinceID(int id) {
            string query = string.Format("select * from tbl_country where country_id = (select country_id from tbl_province where province_id = '{0}')", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Edit(int v, string text, int status) {
            DB.Conn.CatalogUpdate("country", text, v, status);
        }
    }
}
