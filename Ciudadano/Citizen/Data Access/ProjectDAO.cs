﻿using Citizen.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class ProjectDAO {
        public DataTable Search(string col, string filter) {
            string query = string.Format("select * from tbl_project where {0} = '{1}'", col, filter);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchByCategory(int category) {
            string query = string.Format("Select * FROM Tbl_Project WHERE category_id = {0} AND forum_id = {1}", category, Community.Forum.Forum_id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchMyProjects(string username) {
            string query = string.Format("Select * FROM Tbl_Project WHERE username = '{0}' AND forum_id =  {1}", username, Community.Forum.Forum_id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public void Insert(Project project) {
            DB.Conn.InsertProject(project);
        }

        internal void InsertVote(Project pr) {
            DB.Conn.InsertVote(pr);
        }

        internal DataTable CountByCanton() {
            string query = "select tbl_canton.name, count(tbl_canton.canton_id)" +
                           " from tbl_project" +
                           " join tbl_forum on (tbl_project.forum_id = tbl_forum.forum_id)" +
                           " join tbl_canton on (tbl_canton.canton_id = tbl_forum.canton_id)" +
                           " group by tbl_canton.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable CountByProvince() {
            string query = "select tbl_province.name, count(tbl_province.province_id)" +
                           " from tbl_project" +
                           " join tbl_forum on (tbl_project.forum_id = tbl_forum.forum_id)" +
                           " join tbl_canton on (tbl_canton.canton_id = tbl_forum.canton_id)" +
                           " join tbl_province on (tbl_province.province_id = tbl_canton.province_id)" +
                           " group by tbl_province.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable DateRange(string start, string end) {
            string query = string.Format("select * from Tbl_Project where forum_id = {2} and creation_DATE between TO_DATE ('{0}', 'dd/mm/yyyy') and TO_DATE ('{1}', 'dd/mm/yyyy')", start, end, Community.Forum.Forum_id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable CountByCountry() {
            string query = "select tbl_country.name, count(tbl_country.country_id)" +
                           " from tbl_project" +
                           " join tbl_forum on (tbl_project.forum_id = tbl_forum.forum_id)" +
                           " join tbl_canton on (tbl_canton.canton_id = tbl_forum.canton_id)" +
                           " join tbl_province on (tbl_province.province_id = tbl_canton.province_id)" +
                           " Join tbl_country on (tbl_country.country_id = tbl_province.country_id)" +
                           "  group by tbl_country.name";
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable SearchByCategoryName(string selectedText) {
            string query = string.Format("Select * FROM Tbl_Project WHERE CATEGORY_id = {0} AND forum_id = {1}", new CategoryBUS().GetCategoryByName(selectedText).Category_id, Community.Forum.Forum_id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable GetProjectCountByCategory() {
            string query = "SELECT category_id, count(project_id) FROM tbl_project GROUP by category_id";
            return DB.Conn.ExecuteSelectQuery(query);
        }
    }
}
