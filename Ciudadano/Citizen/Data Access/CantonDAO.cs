﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen.Data_Access {
    class CantonDAO {

        public DataTable SearchCantonByProvinceID(int id) {
            string query = string.Format("select * from tbl_canton where province_id = '{0}' order by canton_id asc", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchAll() {
            string query = string.Format("select * from tbl_canton  order by canton_id asc");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public DataTable SearchCantonByDistrictID(int id) {
            string query = string.Format("select * from tbl_canton where canton_id = (select canton_id from tbl_district where district_id = '{0}')", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal void Delete(int v) {
            DB.Conn.CatalogSimpleModInt("canton", "delete", v);
        }
    }
}
