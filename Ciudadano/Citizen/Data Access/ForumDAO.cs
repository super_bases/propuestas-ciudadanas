﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Citizen.Data_Access {
    class ForumDAO {
        public DataTable SearchByCantonID(int id) {
            string query = string.Format("select * from tbl_forum where canton_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }

        public void Insert(Forum forum) {

        }

        internal DataTable GetAll() {
            string query = string.Format("select * from tbl_forum");
            return DB.Conn.ExecuteSelectQuery(query);
        }

        internal DataTable GetForumByCantonID(int id) {
            string query = string.Format("select * from tbl_forum where canton_id = '{0}'", id);
            return DB.Conn.ExecuteSelectQuery(query);
        }
    }
}
