﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Favourite : Audit {
        public int Favourite_id { get; set; }
        public string Favourite_name { get; set; }
        public int Category_id { get; set; }

    }
}
