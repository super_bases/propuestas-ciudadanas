﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class PasswordLog : Audit {
        public int Passwordlog_id { get; set; }
        public string Username { get; set; }
        public string Last_pass { get; set; }
        public string New_pass { get; set; }
        public string Change_date { get; set; }

    }
}
