﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    public abstract class Location : Audit {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
