﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Nationality : Audit {
        public int Nationality_id { get; set; }
        public string Nationality_name { get; set; }
        public int Status { get; set; }

    }
}
