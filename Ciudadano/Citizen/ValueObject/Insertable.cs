﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {

    public abstract class Audit {
        public string Creation_date { get; set; }
        public string Creation_user { get; set; }
        public string Last_modified { get; set; }
        public string Last_modified_user { get; set; }

    }
}
