﻿namespace Citizen {
    internal class Canton : Location {
        public int Canton_id { get; set; }
        public string Canton_name { get; set; }
        public float Lat { get; set; }
        public float Longt { get; set; }
        public int Province_id { get; set; }

    }
}