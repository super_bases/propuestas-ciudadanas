﻿namespace Citizen {
    internal class User : Audit {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Type_id { get; set; }

    }

    internal class UserType : Audit {
        public int Usertype_id { get; set; }
        public int Status { get; set; }
        public string Usertype_name { get; set; }

    }
}