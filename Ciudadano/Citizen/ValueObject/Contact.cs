﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class EmailAddress : Audit {
        public string Email { get; set; }
        public int Email_id { get; set; }
        public int Person_id { get; set; }

    }

    class PhoneNumber : Audit {

        private int phonenumber_id;
        private int phone;
        private int person_id;

        public int Phonenumber_id { get => phonenumber_id; set => phonenumber_id = value; }
        public int Phone { get => phone; set => phone = value; }
        public int Person_id { get => person_id; set => person_id = value; }

    }
}
