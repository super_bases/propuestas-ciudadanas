﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Project : Audit {
        public int Project_id { get; set; }
        public string Project_name { get; set; }
        public string Summary { get; set; }
        public float Budget { get; set; }
        public int Votes { get; set; }
        public string Username { get; set; }
        public int Category_id { get; set; }
        public int Forum_id { get; set; }
    }
}
