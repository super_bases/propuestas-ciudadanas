﻿namespace Citizen {
    internal class Category : Audit {
        public int Category_id { get; set; }
        public string Category_name { get; set; }
        public int Status { get; set; }

    }
}