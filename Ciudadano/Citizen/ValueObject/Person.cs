﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Person : Audit {

        public int Person_id { get; set; }
        public string Identification { get; set; }
        public string Name { get; set; }
        public string First_lastname { get; set; }
        public string Second_lastname { get; set; }
        public string Date { get; set; }
        public string Picture { get; set; }
        public string Username { get; set; }
        public int District_id { get; set; }

    }
}
