﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Votes_Log : Audit {
        public int Votes_id { get; set; }
        public int Last_votes { get; set; }
        public int Current_votes { get; set; }
    }
}
