﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citizen {
    class Message : Audit {
        public int Message_id { get; set; }
        public string Text { get; set; }
        public string Publish_date { get; set; }
        public string Username { get; set; }
        public int Project_id { get; set; }

    }
}
