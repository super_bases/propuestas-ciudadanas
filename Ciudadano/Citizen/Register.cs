﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Citizen {
    public partial class Register : MetroForm {
        ProfileMetroPanel profile;
        public Register() {
            InitializeComponent();
            profile = new ProfileMetroPanel(ProfileMode.New) {
                Location = new Point(23, 63)
            };
            Controls.Add(profile);
        }
    }
}
