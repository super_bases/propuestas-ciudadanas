﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Citizen {
    public partial class CorrectMsg : MetroForm {
        public CorrectMsg(string text) {
            InitializeComponent();
            lblText.Text = text;
        }

        private void metroLabel1_Click(object sender, EventArgs e) {

        }

        private void btnOK_Click(object sender, EventArgs e) {
            Dispose();
        }
    }
}
