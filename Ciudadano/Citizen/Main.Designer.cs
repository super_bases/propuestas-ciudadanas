﻿namespace Citizen {
    partial class Main {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.sidePanel = new MetroFramework.Controls.MetroPanel();
            this.btnStats = new MetroFramework.Controls.MetroTile();
            this.btnLogout = new MetroFramework.Controls.MetroTile();
            this.btnCatalogs = new MetroFramework.Controls.MetroTile();
            this.btnJobs = new MetroFramework.Controls.MetroTile();
            this.btnUsers = new MetroFramework.Controls.MetroTile();
            this.btnTop = new MetroFramework.Controls.MetroTile();
            this.btnProjects = new MetroFramework.Controls.MetroTile();
            this.btnProfile = new MetroFramework.Controls.MetroTile();
            this.mainPanel = new MetroFramework.Controls.MetroPanel();
            this.sidePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sidePanel
            // 
            this.sidePanel.AccessibleName = "sidePanel";
            this.sidePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.sidePanel.Controls.Add(this.btnStats);
            this.sidePanel.Controls.Add(this.btnLogout);
            this.sidePanel.Controls.Add(this.btnCatalogs);
            this.sidePanel.Controls.Add(this.btnJobs);
            this.sidePanel.Controls.Add(this.btnUsers);
            this.sidePanel.Controls.Add(this.btnTop);
            this.sidePanel.Controls.Add(this.btnProjects);
            this.sidePanel.Controls.Add(this.btnProfile);
            this.sidePanel.HorizontalScrollbarBarColor = true;
            this.sidePanel.HorizontalScrollbarHighlightOnWheel = false;
            this.sidePanel.HorizontalScrollbarSize = 12;
            this.sidePanel.Location = new System.Drawing.Point(31, 78);
            this.sidePanel.Margin = new System.Windows.Forms.Padding(4);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(267, 644);
            this.sidePanel.TabIndex = 0;
            this.sidePanel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.sidePanel.VerticalScrollbarBarColor = true;
            this.sidePanel.VerticalScrollbarHighlightOnWheel = false;
            this.sidePanel.VerticalScrollbarSize = 13;
            // 
            // btnStats
            // 
            this.btnStats.Location = new System.Drawing.Point(0, 485);
            this.btnStats.Margin = new System.Windows.Forms.Padding(4);
            this.btnStats.Name = "btnStats";
            this.btnStats.Size = new System.Drawing.Size(267, 73);
            this.btnStats.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnStats.TabIndex = 8;
            this.btnStats.Text = "Stats";
            this.btnStats.Click += new System.EventHandler(this.BtnStats_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(0, 566);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(267, 73);
            this.btnLogout.Style = MetroFramework.MetroColorStyle.Silver;
            this.btnLogout.TabIndex = 9;
            this.btnLogout.Text = "Log out";
            this.btnLogout.Click += new System.EventHandler(this.BtnAbout_Click);
            // 
            // btnCatalogs
            // 
            this.btnCatalogs.Location = new System.Drawing.Point(0, 404);
            this.btnCatalogs.Margin = new System.Windows.Forms.Padding(4);
            this.btnCatalogs.Name = "btnCatalogs";
            this.btnCatalogs.Size = new System.Drawing.Size(267, 73);
            this.btnCatalogs.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnCatalogs.TabIndex = 7;
            this.btnCatalogs.Text = "Catalogs";
            this.btnCatalogs.Click += new System.EventHandler(this.BtnCatalogs_Click);
            // 
            // btnJobs
            // 
            this.btnJobs.Location = new System.Drawing.Point(0, 161);
            this.btnJobs.Margin = new System.Windows.Forms.Padding(4);
            this.btnJobs.Name = "btnJobs";
            this.btnJobs.Size = new System.Drawing.Size(267, 73);
            this.btnJobs.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnJobs.TabIndex = 5;
            this.btnJobs.Text = "Most Voted";
            this.btnJobs.Click += new System.EventHandler(this.BtnJobs_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.Location = new System.Drawing.Point(0, 323);
            this.btnUsers.Margin = new System.Windows.Forms.Padding(4);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(267, 73);
            this.btnUsers.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnUsers.TabIndex = 6;
            this.btnUsers.Text = "Users";
            this.btnUsers.Click += new System.EventHandler(this.BtnUsers_Click);
            // 
            // btnTop
            // 
            this.btnTop.Location = new System.Drawing.Point(0, 242);
            this.btnTop.Margin = new System.Windows.Forms.Padding(4);
            this.btnTop.Name = "btnTop";
            this.btnTop.Size = new System.Drawing.Size(267, 73);
            this.btnTop.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnTop.TabIndex = 4;
            this.btnTop.Text = "Top Communities";
            this.btnTop.Click += new System.EventHandler(this.BtnTop_Click);
            // 
            // btnProjects
            // 
            this.btnProjects.Location = new System.Drawing.Point(0, 80);
            this.btnProjects.Margin = new System.Windows.Forms.Padding(4);
            this.btnProjects.Name = "btnProjects";
            this.btnProjects.Size = new System.Drawing.Size(267, 73);
            this.btnProjects.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnProjects.TabIndex = 3;
            this.btnProjects.Text = "Community Proposals";
            this.btnProjects.Click += new System.EventHandler(this.BtnProjects_Click);
            // 
            // btnProfile
            // 
            this.btnProfile.Location = new System.Drawing.Point(0, 0);
            this.btnProfile.Margin = new System.Windows.Forms.Padding(4);
            this.btnProfile.Name = "btnProfile";
            this.btnProfile.Size = new System.Drawing.Size(267, 73);
            this.btnProfile.Style = MetroFramework.MetroColorStyle.Teal;
            this.btnProfile.TabIndex = 2;
            this.btnProfile.Text = "Profile";
            this.btnProfile.Click += new System.EventHandler(this.BtnProfile_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.HorizontalScrollbarBarColor = true;
            this.mainPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.mainPanel.HorizontalScrollbarSize = 12;
            this.mainPanel.Location = new System.Drawing.Point(301, 78);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(4);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Padding = new System.Windows.Forms.Padding(4);
            this.mainPanel.Size = new System.Drawing.Size(868, 644);
            this.mainPanel.TabIndex = 1;
            this.mainPanel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mainPanel.VerticalScrollbarBarColor = true;
            this.mainPanel.VerticalScrollbarHighlightOnWheel = false;
            this.mainPanel.VerticalScrollbarSize = 13;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 738);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.sidePanel);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1200, 738);
            this.Name = "Main";
            this.Padding = new System.Windows.Forms.Padding(27, 74, 27, 25);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroForm.MetroFormShadowType.None;
            this.Style = MetroFramework.MetroColorStyle.Teal;
            this.Text = "BuiBa";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_Closed);
            this.sidePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel sidePanel;
        private MetroFramework.Controls.MetroPanel mainPanel;
        private MetroFramework.Controls.MetroTile btnTop;
        private MetroFramework.Controls.MetroTile btnProjects;
        private MetroFramework.Controls.MetroTile btnProfile;
        private MetroFramework.Controls.MetroTile btnCatalogs;
        private MetroFramework.Controls.MetroTile btnUsers;
        private MetroFramework.Controls.MetroTile btnJobs;
        private MetroFramework.Controls.MetroTile btnStats;
        private MetroFramework.Controls.MetroTile btnLogout;
    }
}