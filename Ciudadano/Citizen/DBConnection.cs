﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Citizen;
using Oracle.ManagedDataAccess.Client;

public class DBConnection {
    public DataTable ExecuteSelectQuery(string query) {
        OracleConnection conn = new OracleConnection(ConnString());
        conn.Open();

        DataTable data = new DataTable();

        OracleCommand cmd = new OracleCommand(query, conn);
        cmd.CommandType = CommandType.Text;
        cmd.Connection = conn;

        using (OracleDataAdapter dataAdapter = new OracleDataAdapter()) {
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(data);
        }
        return data;
    }


    internal void CatalogSimpleModString(string table, string mod, string name) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_{1}.p_{1}_{0}", table, mod),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add(string.Format("p_{0}_name", table), OracleDbType.Varchar2).Value = name;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    internal void CatalogUpdate(string table, string name, int id, int status) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_update.p_update_{0}", table),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add(string.Format("p_{0}_id", table), OracleDbType.Int32).Value = id;
                cmd.Parameters.Add(string.Format("p_{0}_name", table), OracleDbType.Varchar2).Value = name;
                cmd.Parameters.Add(string.Format("p_status"), OracleDbType.Int32).Value = status;

                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    internal void CatalogTwoString(string table, string mod, string name, string fk_name, int fk_id) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_{1}.p_{1}_{0}", table, mod),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add(string.Format("p_{0}_name", table), OracleDbType.Varchar2).Value = name;
                cmd.Parameters.Add(string.Format("p_{0}_id", fk_name), OracleDbType.Int32).Value = fk_id;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    internal void CatalogSimpleModInt(string table, string mod, int id) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_{1}.p_{1}_{0}", table, mod),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add(string.Format("p_category_{0}", table), OracleDbType.Int32).Value = id;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    // procedure p_update_project (p_project_id in number, p_votes in number);

    internal void InsertVote(Project pr) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_update.p_update_project"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_budget", OracleDbType.Int32).Value = pr.Project_id;
                cmd.Parameters.Add("p_votes", OracleDbType.Int32).Value = pr.Votes;

                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    internal void InsertComment(Message msg) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_insert.p_insert_message"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_text", OracleDbType.Varchar2).Value = msg.Text;
                cmd.Parameters.Add("p_publishdate", OracleDbType.Varchar2).Value = msg.Publish_date;
                cmd.Parameters.Add("p_username", OracleDbType.Varchar2).Value = msg.Username;
                cmd.Parameters.Add("p_project_id", OracleDbType.Int32).Value = msg.Project_id;

                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    internal void InsertProject(Project project) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_insert.p_insert_project"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_projectname", OracleDbType.Varchar2).Value = project.Project_name;
                cmd.Parameters.Add("p_summary", OracleDbType.Varchar2).Value = project.Summary;
                cmd.Parameters.Add("p_budget", OracleDbType.Int32).Value = project.Budget;
                cmd.Parameters.Add("p_p_votes", OracleDbType.Int32).Value = project.Votes;
                cmd.Parameters.Add("p_username", OracleDbType.Varchar2).Value = project.Username;
                cmd.Parameters.Add("p_category_id", OracleDbType.Int32).Value = project.Category_id;
                cmd.Parameters.Add("p_forum_id", OracleDbType.Int32).Value = project.Forum_id;
                cmd.Parameters.Add("p_projectdate", OracleDbType.Varchar2).Value = project.Creation_date;

                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }


    internal void PersonSimpleModString(string identification, string name, 
                                        string flastname, string slastname, string birthdate, string picture, 
                                        string username, int district_id, int nationality_id, string email, 
                                        int phonenumber) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_insert.p_insert_person"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_identification", OracleDbType.Varchar2).Value = identification;
                cmd.Parameters.Add("p_name", OracleDbType.Varchar2).Value = name;
                cmd.Parameters.Add("p_firstlastname", OracleDbType.Varchar2).Value = flastname;
                cmd.Parameters.Add("p_secondlastname", OracleDbType.Varchar2).Value = slastname;
                cmd.Parameters.Add("p_birthdate", OracleDbType.Varchar2).Value = birthdate;
                cmd.Parameters.Add("p_picture", OracleDbType.Varchar2).Value = picture;
                cmd.Parameters.Add("p_username", OracleDbType.Varchar2).Value = username;
                cmd.Parameters.Add("p_district_id", OracleDbType.Int32).Value = district_id;
                cmd.Parameters.Add("p_nationality_id", OracleDbType.Int32).Value = nationality_id;
                cmd.Parameters.Add("p_email", OracleDbType.Varchar2).Value = email;
                cmd.Parameters.Add("p_phonenumber", OracleDbType.Int32).Value = phonenumber;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    /*
         p_person_id in number, p_identification in varchar2, p_name in varchar2, p_firstlastname in varchar2, 
p_secondlastname in varchar2, p_birthdate in varchar2, p_picture in varchar2, p_district_id in number, 
p_nationality_id in number, p_email_id in number, p_email in varchar2, p_phonenumber_id in number, p_phonenumber in number)*/

    internal void updatePerson(int id, string identification, string name, string flastname, string slastname, 
                               string birthdate, string picture, int district_id, int nationality_id, 
                               int email_id, string email, int phone_id, int phonenumber) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_update.p_update_person"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_person_id", OracleDbType.Int32).Value = id;
                cmd.Parameters.Add("p_identification", OracleDbType.Varchar2).Value = identification;
                cmd.Parameters.Add("p_name", OracleDbType.Varchar2).Value = name;
                cmd.Parameters.Add("p_firstlastname", OracleDbType.Varchar2).Value = flastname;
                cmd.Parameters.Add("p_secondlastname", OracleDbType.Varchar2).Value = slastname;
                cmd.Parameters.Add("p_birthdate", OracleDbType.Varchar2).Value = birthdate;
                cmd.Parameters.Add("p_picture", OracleDbType.Varchar2).Value = picture;
                cmd.Parameters.Add("p_district_id", OracleDbType.Int32).Value = district_id;
                cmd.Parameters.Add("p_nationality_id", OracleDbType.Int32).Value = nationality_id;
                cmd.Parameters.Add("p_email_id", OracleDbType.Int32).Value = email_id; 
                cmd.Parameters.Add("p_email", OracleDbType.Varchar2).Value = email;
                cmd.Parameters.Add("p_phonenumber_id", OracleDbType.Int32).Value = phone_id;
                cmd.Parameters.Add("p_phonenumber", OracleDbType.Int32).Value = phonenumber;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }



    internal void updatePassWord(string username, string newPass) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_update.p_update_userPass"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_username", OracleDbType.Varchar2).Value = username;
                cmd.Parameters.Add("p_new_pass", OracleDbType.Varchar2).Value = newPass;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }


    internal void UserSimpleModString(string username, string password, int type) {
        OracleConnection conn = new OracleConnection(ConnString()); ;
        try {
            conn.Open();
            using (OracleCommand cmd = new OracleCommand() {
                Connection = conn,
                CommandText = string.Format("pck_insert.p_insert_user"),
                CommandType = CommandType.StoredProcedure
            }) {
                cmd.Parameters.Add("p_username", OracleDbType.Varchar2).Value = username;
                cmd.Parameters.Add("p_password", OracleDbType.Varchar2).Value = password;
                cmd.Parameters.Add("p_type_id", OracleDbType.Int32).Value = type;
                cmd.ExecuteNonQuery();
            }
        } catch (Exception e) {
            Debug.WriteLine("Se cayo esto {0}", e.Message);
        }
        conn.Close();
    }

    public string ConnString() {
        return String.Format(
            "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)" +
            "(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=bdProyecto)));User Id=PR1;Password=PR1;");

        /*  return String.Format(
            "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)" +
            "(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=database)));User Id=PR1;Password=PR1;");*/
    }


}
